#!/bin/bash

mkdir depends && cd depends
git clone https://gitlab.com/desktop-frameworks/wayqt
git clone https://gitlab.com/desktop-frameworks/utils
git clone https://gitlab.com/desktop-frameworks/settings
git clone https://gitlab.com/desktop-frameworks/ipc
git clone https://gitlab.com/desktop-frameworks/applications
git clone https://gitlab.com/desktop-frameworks/status-notifier
git clone https://gitlab.com/desktop-frameworks/xdg
git clone https://gitlab.com/desktop-frameworks/layouts
git clone https://gitlab.com/desktop-frameworks/login1
git clone https://gitlab.com/desktop-frameworks/inotify
git clone https://gitlab.com/DesQ/libdesq
git clone https://gitlab.com/DesQ/libdesqui
