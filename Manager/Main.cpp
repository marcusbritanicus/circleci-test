/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include <QtCore>
#include <QtDBus>

#include <desq/desq-config.h>

#include <DFL/DF5/Application.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Xdg.hpp>

#include <wayqt/Application.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "Global.hpp"
#include "ShellManager.hpp"

DFL::Settings *shellSett  = nullptr;
WQt::Registry *wlRegistry = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Cask.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Cask started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Shell" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-shell.desktop" );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "l", "list-processes" }, "Run an executable as a tracked process" } );
    parser.addOption( { { "s", "start-process" }, "Run an executable/desq utility as a tracked process", "name" } );
    parser.addOption( { { "r", "restart-process" }, "Restart an alerady running tracked process", "process" } );
    parser.addOption( { { "k", "kill-process" }, "Kill a running tracked process", "process" } );

    parser.process( app );

    if ( app.lockApplication() ) {
        shellSett  = new DFL::Settings( "DesQ", "Shell", ConfigPath );
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );

        wlRegistry->setup();

        DesQ::ShellManager manager;

        QObject::connect( &app, &DFL::Application::messageFromClient, &manager, &DesQ::ShellManager::handleMessages );

        /* Another instance already exists? */
        if ( not manager.isServiceRunning() and not manager.isObjectRegistered() ) {
            qDebug() << "It appears that another instance of desq-shell is already active.";
            qDebug() << "You may use dbus to communicate with it. (Service Name: org.DesQ.Shell)";

            return 1;
        }

        /* Some one else has taken this service!! Grrr... */
        else if ( not manager.isServiceRunning() ) {
            qDebug() << "Unable to start the service 'org.DesQ.Shell'.";
            qDebug() << "Please ensure that another instance of this app is not running elsewhere.";

            return 1;
        }

        /* Something went wrong... */
        else if ( not manager.isObjectRegistered() ) {
            qDebug() << "Unable to register the object '/org/DesQ/Shell'.";
            qDebug() << "Please ensure that another instance of this app is not running elsewhere.";

            return 1;
        }

        /* All is well. */
        else {
            qDebug() << "Service org.DesQ.Shell running...";
            qDebug() << "Object /org/DesQ/Shell registered...";
            qDebug() << "Beginning Shell Management";
        }

        manager.startManagement();

        return app.exec();
    }

    else {
        if ( parser.isSet( "list-processes" ) ) {
            QObject::connect(
                &app, &DFL::Application::messageFromServer, [ = ]( QString msg ) {
                    QStringList procs( msg.split( "\n" ) );
                    printf( "DesQ Shell " PROJECT_VERSION "\n\n" );
                    printf( "Currently, the following processes are running:\n" );
                    for ( QString proc: procs ) {
                        printf( "  => %s\n", proc.toUtf8().constData() );
                    }
                    printf( "\n" );

                    /**
                     * We cannot use return 0 => that will take us to app.exec();
                     * std::exit( 0 ) will exit the app with exit code = 0.
                     */
                    std::exit( 0 );
                }
            );

            app.messageServer( "list-processes" );

            /** Wait till we recieve a reply, and display it. */
            app.exec();
        }

        else if ( parser.isSet( "start-process" ) ) {
            QString proc = parser.value( "start-process" );
            app.messageServer( "start-process\n\n" + proc + "\n\n" + parser.positionalArguments().join( "\n" ) );
        }

        else if ( parser.isSet( "restart-process" ) ) {
            QString proc = parser.value( "restart-process" );
            app.messageServer( "restart-process\n" + proc );
        }

        else if ( parser.isSet( "kill-process" ) ) {
            QString proc = parser.value( "kill-process" );
            app.messageServer( "kill-process\n" + proc );
        }

        else {
            parser.showHelp();
        }

        return 0;
    }

    return 0;
}
