/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore/QObject>
#include <QtDBus/QtDBus>
#include <QtCore/qcontainerfwd.h>

class ShellAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT;
    Q_CLASSINFO( "D-Bus Interface", "org.DesQ.Shell" );
    Q_CLASSINFO(
        "D-Bus Introspection",
        "<interface name='org.DesQ.Shell'>"
        "   <method name='Shutdown'/>\n"
        "   <method name='ChangeBrightness'>\n"
        "       <arg name=\"service\" type=\"float\" direction=\"in\"/>\n"
        "   </method>\n"
        "   <method name='ChangeVolume'>\n"
        "       <arg name=\"service\" type=\"float\" direction=\"in\"/>\n"
        "   </method>\n"
        "</interface>"
    );

    public:
        ShellAdaptor( QObject *parent );
        virtual ~ShellAdaptor();

    public Q_SLOTS:
        void Shutdown();
};
