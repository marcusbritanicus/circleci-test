/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <ctime>

#include "Global.hpp"
#include "ShellManager.hpp"
#include "AutoStart.hpp"
#include "TrackedProcess.hpp"
#include "ShellAdaptor.hpp"

#include <desq/Xdg.hpp>
#include <desq/Utils.hpp>
#include <DFL/DF5/Settings.hpp>
#include <desq/desq-config.h>

#include <DFL/DF5/Application.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayfireShell.hpp>

int MaxAutoRestartLimit;

static inline QString getSocketName( QString appId ) {
    QString sockName;

    /* $__DESQ_WORK_DIR is created and set by the session */
    if ( qgetenv( "__DESQ_WORK_DIR" ).count() ) {
        sockName = QString( qgetenv( "__DESQ_WORK_DIR" ) ) + "/" + appId;
    }

    /* It does not exist: Let's create it */
    else {
        /* We're going to assume that XDG_RUNTIME_DIR exists and is writable at this point */
        QString XDG_RUNTIME_DIR = DesQ::XDG::xdgRuntimeDir();

        /* We're also going to assume that XDG_SESSION_ID exists */
        QString XDG_SESSION_ID( qgetenv( "XDG_SESSION_ID" ) );

        /* Create this path blindly. This MUST work: No fallbacks */
        QDir( XDG_RUNTIME_DIR ).mkpath( "DesQSession-" + XDG_SESSION_ID + "/" );

        /* Our socket lives here */
        sockName = QDir( XDG_RUNTIME_DIR ).filePath( "DesQSession-" + XDG_SESSION_ID + "/" + appId );
    }

    return sockName;
}


DesQ::ShellManager::ShellManager() : QObject() {
    MaxAutoRestartLimit = shellSett->value( "Manager/MaxAutoRestartLimit" );

    qDebug( "Creating ShellAdaptor" );
    new ShellAdaptor( this );

    qDebug( "Registering DesQ Shell Service/Object" );
    QDBusConnection shellDBus = QDBusConnection::sessionBus();

    serviceRunning   = shellDBus.registerService( "org.DesQ.Shell" );
    objectRegistered = shellDBus.registerObject( "/org/DesQ/Shell", this );
}


DesQ::ShellManager::~ShellManager() {
    shutdown();
}


bool DesQ::ShellManager::isServiceRunning() {
    return serviceRunning;
}


bool DesQ::ShellManager::isObjectRegistered() {
    return objectRegistered;
}


void DesQ::ShellManager::startManagement() {
    /** Tracked processes specified in the User Config */
    QStringList utils = shellSett->value( "Utilities" );

    for ( QString util: utils ) {
        qDebug() << "Starting" << util;

        QString  name( shellSett->value( util + "/Exec" ) );
        QVariant args = shellSett->value( util + "/Args" );

        startProcess( name, args.toStringList() );
    }

    // Synthetic 5s delay.
    int t = 0;

    while ( t < 10 ) {
        // Sleep of 500 ms at one time
        // 500 (ms) * 1000 (us per ms) * 1000 (ns per us)
        struct timespec ts = { 0, 500 * 1000 * 1000 };
        nanosleep( &ts, NULL );
        qApp->processEvents();
        t++;
    }

    qDebug() << "Starting auto-start apps";
    DesQAutoStart *autoStart = new DesQAutoStart();

    autoStart->start();

    qDebug() << "Activating hot-corners";
    setupHotCorners();
}


void DesQ::ShellManager::listProcesses( int fd ) {
    qApp->messageClient( activatedUtils.keys().join( "\n" ), fd );
}


void DesQ::ShellManager::startProcess( QString util, QStringList args ) {
    /** Do nothing if it's already running */
    if ( activatedUtils.contains( util ) ) {
        return;
    }

    QString utilExec = DesQ::Utils::getUtilityPath( util );

    if ( DesQ::Utils::isExecutable( utilExec ) ) {
        TrackedProcess *tp = new TrackedProcess( utilExec, args );
        tp->start();

        connect(
            tp, &TrackedProcess::crashed, [ = ] () mutable {
                if ( activatedUtils.contains( util ) ) {
                    activatedUtils.take( util )->deleteLater();
                }
            }
        );

        activatedUtils[ util ] = tp;
    }
}


void DesQ::ShellManager::killProcess( QString util ) {
    /** Do nothing if it's not running */
    if ( not activatedUtils.contains( util ) ) {
        return;
    }

    activatedUtils.take( util )->terminate();
}


void DesQ::ShellManager::restartProcess( QString util ) {
    /** Do nothing if it's not running */
    if ( not activatedUtils.contains( util ) ) {
        return;
    }

    /** Restart otherwise */
    else {
        activatedUtils.value( util )->restart();
    }
}


void DesQ::ShellManager::shutdown() {
    delete shellSett;

    for ( QString util: activatedUtils.keys() ) {
        TrackedProcess *tp = activatedUtils.take( util );

        if ( tp->isRunning() ) {
            tp->terminate();
            delete tp;
        }
    }

    /* Close this daemon */
    qApp->quit();
}


void DesQ::ShellManager::setupHotCorners() {
    for ( QScreen *scrn: qApp->screens() ) {
        setupHotSpotForScreen( scrn );
    }

    connect( qApp, &QApplication::screenAdded, this, &DesQ::ShellManager::setupHotSpotForScreen );
}


void DesQ::ShellManager::setupHotSpotForScreen( QScreen *scrn ) {
    wl_output     *wlout    = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );
    WQt::WfOutput *wfOutput = wlRegistry->wayfireShell()->getOutput( wlout );

    WQt::HotSpot *topLeftHS = wfOutput->createHotSpot( WQt::WfOutput::TopLeft, 1, 250 );

    connect(
        topLeftHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::TopLeft, scrn->name() );
        }
    );
    connect(
        topLeftHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::TopLeft, scrn->name() );
        }
    );
    topLeftHS->setup();

    WQt::HotSpot *topRightHS = wfOutput->createHotSpot( WQt::WfOutput::TopRight, 1, 250 );

    connect(
        topRightHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::TopRight, scrn->name() );
        }
    );
    connect(
        topRightHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::TopRight, scrn->name() );
        }
    );
    topRightHS->setup();

    WQt::HotSpot *bottomLeftHS = wfOutput->createHotSpot( WQt::WfOutput::BottomLeft, 1, 250 );

    connect(
        bottomLeftHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::BottomLeft, scrn->name() );
        }
    );
    connect(
        bottomLeftHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::BottomLeft, scrn->name() );
        }
    );
    bottomLeftHS->setup();

    WQt::HotSpot *bottomRightHS = wfOutput->createHotSpot( WQt::WfOutput::BottomRight, 1, 250 );

    connect(
        bottomRightHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::BottomRight, scrn->name() );
        }
    );
    connect(
        bottomRightHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::BottomRight, scrn->name() );
        }
    );
    bottomRightHS->setup();
}


void DesQ::ShellManager::handleHotCorner( int corner, QString scrn ) {
    /* Based on the corner take action */
    switch ( corner ) {
        case WQt::WfOutput::TopLeft: {
            qDebug() << "Entering Top Left hot Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::TopRight: {
            qDebug() << "Entering Top Right Corner of" << scrn;
            /* Use TasksPlugin to show the list widget */
            break;
        }

        case WQt::WfOutput::BottomLeft: {
            qDebug() << "Entering Bottom Left Corner";
            QDBusInterface iface( "org.DesQ.Menu", "/org/DesQ/Menu", "org.DesQ.Menu", QDBusConnection::sessionBus() );
            iface.call( "Toggle" );

            break;
        }

        case WQt::WfOutput::BottomRight: {
            qDebug() << "Entering Bottom Right Corner";
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "cask" ), { "--raise", "--output", scrn } );
            break;
        }

        /* In case hot corner was canceled */
        default: {
            break;
        }
    }
}


void DesQ::ShellManager::cancelHotCorner( int corner, QString scrn ) {
    switch ( corner ) {
        case WQt::WfOutput::TopLeft: {
            qDebug() << "Leaving Top Left Corner of" << scrn;
            break;
        }

        /* We are exiting from the TopRight corner, so show restore the window state */
        case WQt::WfOutput::TopRight: {
            qDebug() << "Leaving Top Right Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::BottomLeft: {
            qDebug() << "Leaving Bottom Left Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::BottomRight: {
            qDebug() << "Leaving Bottom Right Corner";
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "cask" ), { "--lower", "--output", scrn } );
            break;
        }

        /* In case hot corner was canceled */
        default: {
            break;
        }
    }
}


void DesQ::ShellManager::handleMessages( QString message, int fd ) {
    if ( message == "list-processes" ) {
        listProcesses( fd );
    }

    else if ( message.startsWith( "start-process" ) ) {
        QStringList parts = message.split( "\n\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        startProcess( parts[ 0 ], (parts.count() == 2 ? parts.at( 1 ).split( "\n" ) : QStringList() ) );
    }

    else if ( message.startsWith( "restart-process" ) ) {
        QStringList parts = message.split( "\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        restartProcess( parts[ 0 ] );
    }

    else if ( message.startsWith( "kill-process" ) ) {
        QStringList parts = message.split( "\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        killProcess( parts[ 0 ] );
    }
}
