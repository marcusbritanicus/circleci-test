/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QThread>
#include <QString>
#include <QProcess>
#include <QSettings>
#include <QDateTime>

extern int MaxAutoRestartLimit;

/**
 * TrackedProcess - A subclass of QProcess
 * We will track the app till it closes, then restart it.
 * If the app closed/crashed within one minute, increment
 * the count, otherwise reset the count to 0.
 * If we hit 5 crashes within a minute, we will inform
 * the user, before attempting a restart.
 **/

class TrackedProcess : public QObject {
    Q_OBJECT;

    public:
        TrackedProcess( QString path, QStringList args );
        ~TrackedProcess();

        void start();
        void restart();

        int restartCount();
        bool isRunning();

    public Q_SLOTS:
        void terminate();

    private:
        void tryRestart();

        int mCrashCount = 0;
        QString mExecPath;
        QStringList mArgs;

        QProcess *proc;
        QDateTime mStartTime;

    Q_SIGNALS:
        void crashed();
};
