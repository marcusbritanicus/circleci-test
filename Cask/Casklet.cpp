/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Casklet.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

// DesQ Headers
#include <desqui/ShellPlugin.hpp>

DesQ::Cask::Casklet::Casklet( QString widget ) {
    QSettings wSett( MetadataPath + widget + ".ini", QSettings::IniFormat );

    mPluginName = widget;
    mWidgetName = wSett.value( "Name" ).toString();
    mWidgetDesc = wSett.value( "Descrpition" ).toString();

    setToolTip( mWidgetDesc );
    setAttribute( Qt::WA_TranslucentBackground );

    setFixedWidth( 200 );

    /* To show the widget name and the error message */
    setMinimumHeight( 50 );

    /** Layout */
    lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins( 2, 20, 2, 2 ) );
    setLayout( lyt );

    QTimer::singleShot( 1000, this, &DesQ::Cask::Casklet::prepareWidget );
}


void DesQ::Cask::Casklet::reload() {
    delete pluginWidget;
    pluginWidget = nullptr;

    prepareWidget();
}


void DesQ::Cask::Casklet::prepareWidget() {
    mState = State::Loading;
    repaint();
    qApp->processEvents();

    QPluginLoader loader( PluginPath "shell/libdesq-plugin-" + mPluginName + ".so" );
    QObject       *pObject = loader.instance();

    pluginWidget = nullptr;

    if ( pObject ) {
        DesQ::Plugin::Shell *plugin = qobject_cast<DesQ::Plugin::Shell *>( pObject );

        if ( !plugin ) {
            qWarning() << "Plugin Error:" << loader.errorString();
            pluginWidget = new QLabel( loader.errorString() );
            mState       = State::Failed;
        }

        else {
            pluginWidget = plugin->widget( this );
            mState       = State::Loaded;
        }
    }

    else {
        qWarning() << "Plugin Error:" << loader.errorString();
        pluginWidget = new QLabel( loader.errorString() );
        mState       = State::Failed;
    }

    lyt->addWidget( pluginWidget );
    setLayout( lyt );

    repaint();
}


void DesQ::Cask::Casklet::resizeContainer() {
    qDebug() << "Not implemented";
}


void DesQ::Cask::Casklet::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QBrush brush( QColor( 0, 0, 0, 180 ) );

    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( brush );
    painter.drawRoundedRect( rect().adjusted( 0, 0, -1, -1 ), 3, 3 );
    painter.restore();

    switch ( mState ) {
        case State::NotLoaded: {
            painter.drawText( rect(), Qt::AlignCenter | Qt::TextWordWrap, "Waiting for widget..." );
            pEvent->accept();
            break;
        }

        case State::Loading: {
            painter.drawText( rect(), Qt::AlignCenter | Qt::TextWordWrap, "Loading the widget..." );
            pEvent->accept();
            break;
        }

        case State::Failed:
        case State::Loaded: {
            /* Nothing from our side */
            painter.drawText( QRect( 0, 3, width(), height() ), Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap, mWidgetName );
            QWidget::paintEvent( pEvent );
            break;
        }
    }

    painter.end();
}
