/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Manager.hpp"

// LibDesQ Headers
#include <desq/Utils.hpp>
#include <desq/desq-config.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <DFL/DF5/Application.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Xdg.hpp>

DFL::Settings *shellSett;
WQt::Registry *wlRegistry;

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Cask.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Cask started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Cask" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-cask.desktop" );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "a", "add-widget" }, "Add a widget to the cask", "widget" } );
    parser.addOption( { { "i", "insert-widget" }, "Insert a widget to the cask", "widget" } );
    parser.addOption( { { "p", "position" }, "Position where the inserted widget goes", "position" } );
    parser.addOption( { { "w", "list-widgets" }, "List of the active widgets" } );
    parser.addOption( { { "x", "remove-widget" }, "Remove a widget from the cask", "widget" } );
    parser.addOption( { { "r", "reload-widget" }, "Reload a running widget", "widget" } );
    parser.addOption( { { "s", "raise" }, "Raise the cask to the top" } );
    parser.addOption( { { "l", "lower" }, "Lower the cask to the bottom" } );
    parser.addOption( { { "t", "toggle" }, "Show/hide the cask instance" } );
    parser.addOption( { { "o", "output" }, "Output on which raise/lower/toggle is done.", "output" } );

    /* Widgets to be loaded */
    parser.addPositionalArgument( "widgets", "Widgets to be loaded", "widgets" );

    parser.process( app );

    if ( app.lockApplication() ) {
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );
        wlRegistry->setup();

        shellSett = new DFL::Settings( "DesQ", "Shell", ConfigPath );

        DesQ::Cask::Manager *caskMgr = new DesQ::Cask::Manager( parser.positionalArguments() );

        QObject::connect( &app, &DFL::Application::messageFromClient, caskMgr, &DesQ::Cask::Manager::handleMessages );

        QObject::connect(
            &app, &QApplication::screenAdded, [ caskMgr ] ( QScreen *screen ) {
                caskMgr->createInstance( screen );
            }
        );

        return app.exec();
    }

    else {
        if ( parser.isSet( "raise" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Cask instance is to be raised.";
                return 1;
            }

            app.messageServer( "raise\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "lower" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Cask instance is to be lowered.";
                return 1;
            }

            app.messageServer( "lower\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "add-widget" ) ) {
            app.messageServer( "add\n" + parser.value( "add-widget" ) );
        }

        else if ( parser.isSet( "insert-widget" ) ) {
            if ( parser.isSet( "position" ) ) {
                app.messageServer( "insert\n" + parser.value( "add-widget" ) + "\n" + parser.value( "position" ) );
            }

            else {
                printf( "DesQ Cask " PROJECT_VERSION "\n" );
                parser.showHelp();
            }
        }

        else if ( parser.isSet( "list-widgets" ) ) {
            QObject::connect(
                &app, &DFL::Application::messageFromServer, [ = ]( QString msg ) {
                    qDebug() << msg;

                    /**
                     * We cannot use return 0 => that will take us to app.exec();
                     * std::exit( 0 ) will exit the app with exit code = 0.
                     */
                    std::exit( 0 );
                }
            );

            app.messageServer( "list-widgets" );

            /** Wait till we recieve a reply, and display it. */
            app.exec();
        }

        else if ( parser.isSet( "remove-widget" ) ) {
            app.messageServer( "remove\n" + parser.value( "remove-widget" ) );
        }

        else if ( parser.isSet( "reload-widget" ) ) {
            app.messageServer( "reload\n" + parser.value( "reload-widget" ) );
        }

        return 0;
    }

    return 0;
}
