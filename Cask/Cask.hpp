/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>
#include <QtGui>
#include <QtCore>
#include <QtSvg>

namespace DesQ {
    namespace Cask {
        class UI;
        class Casklet;
    }
}

class DesQ::Cask::UI : public QWidget {
    Q_OBJECT;

    public:
        UI( QStringList widgets );

        /**
         * Add a widget to the container
         * The added widget will be at the end.
         * Name will be library name withoutthe libdesq-plugin- and the .so part
         * For example: libdesq-plugin-volume.so will be 'volume'
         * This function will simply call insertWidget( <name>, -1 );
         * Adding an existing widget will cause it to be reloaded
         */
        void addWidget( QString name );

        /**
         * Insert a widget to the container at the given position
         * The if the position is -1 or greater than widget count, the widget
         * will be added at the end.
         * Name will be library name without the libdesq-plugin- and the .so part
         * For example: libdesq-plugin-volume.so will be 'volume'
         * This function will simply call insertWidget( <name>, -1 );
         * Inserting an existing widget will cause it to be reloaded
         */
        void insertWidget( QString name, int pos );

        /**
         * Remove the widget
         * Name will be library name without the libdesq-plugin- and the .so part
         */
        void removeWidget( QString name );

        /**
         * Reload the widget
         * Name will be library name without the libdesq-plugin- and the .so part
         */
        void reloadWidget( QString name );

        /**
         * Handle messages passed from the CLI
         */
        void handleMessage( QString, int );

    private:
        void startLayout();

        QStringList mWidgets;
        QVBoxLayout *widgetLyt;

        QHash<QString, DesQ::Cask::Casklet *> widgetMap;
};
