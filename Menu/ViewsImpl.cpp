/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ViewsImpl.hpp"

DesktopParser::DesktopParser() : QThread() {
    // Nothing important to be done
}


void DesktopParser::addToParser( QString app ) {
    if ( not mApps.contains( app ) ) {
        mApps << app;
    }

    /** Auto-start */
    if ( not isRunning() ) {
        start();
    }
}


void DesktopParser::parseDesktop( QString app ) {
    DFL::XDG::DesktopFile desktop( app );

    if ( not desktop.isValid() or not desktop.visible() ) {
        return;
    }

    if ( desktop.type() != DFL::XDG::DesktopFile::Application ) {
        return;
    }

    QStandardItem *item = new QStandardItem();

    item->setIcon( QIcon::fromTheme( desktop.icon(), QIcon::fromTheme( "application-x-desktop" ) ) );
    item->setText( desktop.name() );

    QString descr( desktop.description() );

    if ( descr.isEmpty() ) {
        descr = desktop.genericName();
    }

    item->setToolTip( QString( "<big>%1</big><br>%2" ).arg( desktop.name() ).arg( descr ) );

    item->setData( desktop.name() + desktop.genericName() + desktop.description(), Qt::UserRole + 1 );
    item->setData( desktop.category(),                                             Qt::UserRole + 2 );
    item->setData( desktop.desktopName(),                                          Qt::UserRole + 3 );

    emit appReady( desktop, item );
}


void DesktopParser::run() {
    while ( mApps.count() ) {
        parseDesktop( mApps.takeAt( 0 ) );
    }
}


QSize AppsDelegate::sizeHint( const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    QSize size( QStyledItemDelegate::sizeHint( option, index ) );

    /* Icon View */
    if ( option.decorationPosition == QStyleOptionViewItem::Top ) {
        // size.setWidth( static_cast<int>(FileSystemView::gridWidth) - 5 );
        return QSize( 128, 96 );
    }

    /* Tile View */
    else if ( option.decorationPosition == QStyleOptionViewItem::Left ) {
        // size.setWidth( static_cast<int>(FileSystemView::gridWidth) - 5 );
        return QSize( 256, 72 );
    }

    return size;
}
