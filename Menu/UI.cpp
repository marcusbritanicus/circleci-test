/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "UI.hpp"
#include "AppsView.hpp"
#include "OtherViews.hpp"
#include "Terminator.hpp"

#include <DFL/DF5/IpcClient.hpp>

DesQ::Menu::UI::UI() : QWidget() {
    /* Title */
    setWindowTitle( "DesQ Menu" );

    /* No frame, and stay on top */
    setWindowFlags( Qt::Popup | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );

    setAttribute( Qt::WA_TranslucentBackground );
}


void DesQ::Menu::UI::setup() {
    searchEdit = new QLineEdit();
    searchEdit->setPlaceholderText( "Press '/' to start search..." );
    searchEdit->setFont( QFont( font().family(), 20 ) );
    searchEdit->setAlignment( Qt::AlignCenter );
    searchEdit->setFrame( QFrame::NoFrame );
    searchEdit->setStyleSheet( "background: transparent; margin-bottom: 10px;" );
    searchEdit->setMinimumWidth( 600 );
    searchEdit->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred ) );

    /** Timer to delay search start */
    delayTimer = new QBasicTimer();

    catsView = new DesQ::Menu::CategoryView( this );
    appsView = new DesQ::Menu::ApplicationsView( this );
    favsView = new DesQ::Menu::FavoritesView( this );

    catsView->setCategories( appsView->availableCategories() );

    connect(
        searchEdit, &QLineEdit::textEdited, [ = ] () {
            delayTimer->start( 250, Qt::PreciseTimer, this );
        }
    );

    connect(
        appsView, &DesQ::Menu::ApplicationsView::updateCategories, [ = ] () {
            catsView->setCategories( appsView->availableCategories() );
        }
    );

    connect( catsView, &DesQ::Menu::CategoryView::loadCategory,  appsView, &DesQ::Menu::ApplicationsView::loadCategory );
    connect( catsView, &DesQ::Menu::CategoryView::closeMenu,     this,     &DesQ::Menu::UI::close );
    connect( appsView, &DesQ::Menu::ApplicationsView::closeMenu, this,     &DesQ::Menu::UI::close );
    connect( favsView, &DesQ::Menu::FavoritesView::closeMenu,    this,     &DesQ::Menu::UI::close );

    connect(
        catsView, &DesQ::Menu::CategoryView::focusSearch, [ = ] () {
            searchEdit->setFocus();
        }
    );

    connect(
        appsView, &DesQ::Menu::ApplicationsView::focusSearch, [ = ] () {
            searchEdit->setFocus();
        }
    );

    connect(
        favsView, &DesQ::Menu::FavoritesView::focusSearch, [ = ] () {
            searchEdit->setFocus();
        }
    );

    QGridLayout *viewLyt = new QGridLayout();

    viewLyt->setContentsMargins( 5, 15, 5, 15 );
    viewLyt->setSpacing( 0 );
    viewLyt->addWidget( catsView, 1, 0 );
    viewLyt->addWidget( appsView, 1, 1 );
    viewLyt->addWidget( favsView, 1, 2 );

    QFrame *menuFrame = new QFrame();

    menuFrame->setLayout( viewLyt );
    menuFrame->setObjectName( "frame" );
    menuFrame->setStyleSheet( "#frame{ border: none; border-top: 1px solid gray;; border-bottom: 1px solid gray; }" );

    QLayout     *logoutLyt = setupLogoutButtons();
    QGridLayout *menuLyt   = new QGridLayout();

    menuLyt->addWidget( searchEdit, 0, 0, 1, 2, Qt::AlignCenter );
    menuLyt->addWidget( menuFrame,  1, 0, 1, 2 );
    menuLyt->addLayout( logoutLyt, 2, 0, Qt::AlignCenter );

    setLayout( menuLyt );

    appsView->setFocus();
}


QLayout * DesQ::Menu::UI::setupLogoutButtons() {
    QString orgName = "DesQ";
    QString appName = "Session Manager";

    QString sockDir( "%1/%2/%3" );

    sockDir = sockDir.arg( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) )
                 .arg( orgName.replace( " ", "" ) )
                 .arg( QString( qgetenv( "XDG_SESSION_ID" ) ) );

    QString sockPath = QString( "%1/%2.socket" ).arg( sockDir ).arg( appName.replace( " ", "" ) );

    DFL::IPC::Client *client = new DFL::IPC::Client( sockPath, this );

    client->connectToServer();

    /** Wait 100 ms for connection */
    if ( not client->waitForRegistered( 10 * 1000 ) ) {
        qCritical() << "Unable to connect to" << sockPath;
    }

    std::function<bool( QString )> canDo = [ client ] ( QString action ) -> bool {
                                               client->sendMessage( "Can" + action );
                                               client->waitForReply( 10 * 1000 );
                                               return (client->reply() == "true" ? true : false);
                                           };

    QToolButton *lockScreenBtn = new QToolButton();

    lockScreenBtn->setIcon( QIcon::fromTheme( "system-lock-screen" ) );
    lockScreenBtn->setIconSize( QSize( 36, 36 ) );
    lockScreenBtn->setToolTip( "Lock the screen" );
    connect(
        lockScreenBtn, &QToolButton::pressed, [ = ] () {
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "lock" ), {} );
        }
    );

    QToolButton *logOutBtn = new QToolButton();

    logOutBtn->setIcon( QIcon::fromTheme( "system-log-out" ) );
    logOutBtn->setIconSize( QSize( 36, 36 ) );
    logOutBtn->setToolTip( "Log out of DesQ" );
    connect(
        logOutBtn, &QToolButton::pressed, [ = ] () {
            closeRunningApps();
            client->sendMessage( "logout" );
        }
    );

    QToolButton *suspendBtn = new QToolButton();

    suspendBtn->setIcon( QIcon::fromTheme( "system-suspend" ) );
    suspendBtn->setIconSize( QSize( 36, 36 ) );
    suspendBtn->setToolTip( "Suspend to RAM" );
    connect(
        suspendBtn, &QToolButton::pressed, [ = ] () {
            client->sendMessage( "suspend" );
        }
    );
    suspendBtn->setDisabled( not canDo( "Suspend" ) );

    QToolButton *hibernateBtn = new QToolButton();

    hibernateBtn->setIcon( QIcon::fromTheme( "system-suspend-hibernate" ) );
    hibernateBtn->setIconSize( QSize( 36, 36 ) );
    hibernateBtn->setToolTip( "Suspend to Disk" );
    connect(
        hibernateBtn, &QToolButton::pressed, [ = ] () {
            client->sendMessage( "hibernate" );
        }
    );
    hibernateBtn->setDisabled( not canDo( "Hibernate" ) );

    QToolButton *powerOffBtn = new QToolButton();

    powerOffBtn->setIcon( QIcon::fromTheme( "system-shutdown" ) );
    powerOffBtn->setIconSize( QSize( 36, 36 ) );
    powerOffBtn->setToolTip( "Shutdown the system" );
    connect(
        powerOffBtn, &QToolButton::pressed, [ = ] () {
            closeRunningApps();
            client->sendMessage( "shutdown" );
        }
    );
    powerOffBtn->setDisabled( not canDo( "PowerOff" ) );

    QToolButton *rebootBtn = new QToolButton();

    rebootBtn->setIcon( QIcon::fromTheme( "system-reboot" ) );
    rebootBtn->setIconSize( QSize( 36, 36 ) );
    rebootBtn->setToolTip( "Reboot the system" );
    connect(
        rebootBtn, &QToolButton::pressed, [ = ] () {
            closeRunningApps();
            client->sendMessage( "reboot" );
        }
    );
    rebootBtn->setDisabled( not canDo( "Reboot" ) );

    QHBoxLayout *powerLyt = new QHBoxLayout();

    powerLyt->addStretch();
    powerLyt->addWidget( lockScreenBtn );
    powerLyt->addWidget( new QLabel( "|" ) );
    powerLyt->addWidget( logOutBtn );
    powerLyt->addWidget( suspendBtn );
    powerLyt->addWidget( hibernateBtn );
    powerLyt->addWidget( new QLabel( "|" ) );
    powerLyt->addWidget( powerOffBtn );
    powerLyt->addWidget( rebootBtn );
    powerLyt->addStretch();

    return powerLyt;
}


void DesQ::Menu::UI::toggleVisible() {
    if ( isVisible() ) {
        hide();
    }

    else {
        showMaximized();
    }
}


void DesQ::Menu::UI::mouseReleaseEvent( QMouseEvent *mEvent ) {
    QWidget::mouseReleaseEvent( mEvent );
    close();
}


void DesQ::Menu::UI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.fillRect( geometry(), QColor( 0, 0, 0, 230 ) );
    painter.end();

    QWidget::paintEvent( pEvent );
}


void DesQ::Menu::UI::closeEvent( QCloseEvent *cEvent ) {
    emit closed();

    /** Close the view first */
    QWidget::closeEvent( cEvent );
    qApp->processEvents();

    /** Clear the search edit */
    searchEdit->clear();

    /** Reset the search on close */
    appsView->setFilter( QString() );

    /** Enable al lthe categories */
    catsView->setEnabled( true );

    /** Reset to default */
    catsView->setCurrentItem( catsView->item( 0 ) );
    catsView->loadCategory( "All Applications" );

    /** Clear all selections and focus the Applications view */
    appsView->selectionModel()->clearSelection();
    appsView->setFocus();
}


void DesQ::Menu::UI::timerEvent( QTimerEvent *tEvent ) {
    if ( delayTimer->timerId() == tEvent->timerId() ) {
        delayTimer->stop();

        QString filter( searchEdit->text() );
        appsView->setFilter( filter );

        if ( filter.isEmpty() ) {
            catsView->setEnabled( true );
        }

        else {
            catsView->setDisabled( true );
        }

        return;
    }

    QWidget::timerEvent( tEvent );
}
