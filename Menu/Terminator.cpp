/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <sys/types.h>
#include <signal.h>

#include <QDir>
#include <QDebug>
#include <QProcess>

#include <QtDBus>

#include <desq/Utils.hpp>
#include "Terminator.hpp"


void closeRunningApps() {
    /**
     * 1. wayland.compositor.output.QueryOutputIds will give us a list of outputs.
     * 2. wayland.compositor.output.QueryOutputViews will give us a list of views of that output.
     * 3. wayland.compositor.views.QueryViewAppId will give us the pid of the view.
     */

    /** output interface */
    QDBusInterface output( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.output", QDBusConnection::sessionBus() );

    /** views interface */
    QDBusInterface views( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.views", QDBusConnection::sessionBus() );

    QList<uint> outputIds;
    QList<uint> viewIds;

    /** Get the outputs */
    QDBusReply<QList<uint> > outputs = output.call( "QueryOutputIds" );

    if ( outputs.isValid() ) {
        outputIds = outputs.value();
    }

    /** Get the views in each output */
    for ( uint outputId: outputIds ) {
        QDBusReply<QList<uint> > viewsreply = output.call( "QueryOutputViews", outputId );

        if ( viewsreply.isValid() ) {
            viewIds << viewsreply.value();
        }
    }

    QStringList appIds;

    for ( uint viewId: viewIds ) {
        QDBusReply<QString> appIdRaw = views.call( "QueryViewAppId", viewId );

        if ( appIdRaw.isValid() and not appIdRaw.value().isEmpty() ) {
            /** Store the AppId */
            appIds << appIdRaw.value();

            /** A request to close the view */
            views.call( "CloseView", viewId );
        }
    }

    /**
     * Save the app ids if it's enabled.
     * Ideally we should retrieve the desktop file for these apps
     * store the last commandline, and the environment.
     * This work is pending and will be completed at  later date.
     */
    QSettings session( "DesQ", "Session" );

    if ( session.value( "SaveSession", false ).toBool() ) {
        session.setValue( "PreviousSession", appIds );
    }
}
