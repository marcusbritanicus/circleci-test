/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <DFL/DF5/Xdg.hpp>
#include <desq/Utils.hpp>

namespace DFL {
    class Inotify;
}

namespace DesQ {
    namespace Menu {
        class ApplicationsView;
    }
}

class DesktopParser;

class DesQ::Menu::ApplicationsView : public QListView {
    Q_OBJECT;

    public:
        ApplicationsView( QWidget *parent );
        ~ApplicationsView();

        void loadCategory( QString category );
        void setFilter( QString filter );

        QStringList availableCategories();

    private:
        QStringList mAvailableCategories;

        QStandardItemModel *appsModel;
        QSortFilterProxyModel *proxyModel;

        /**
         * Parse the desktop files in all the folders of @desktopPaths
         * If the optional @path is not empty, parse that desktop or
         * all the desktops only in that folder.
         */
        void parseDesktops( QStringList paths );
        void removeDesktop( QString path );

        /**
         * When the DesktopParser parses and prepares a QStandardItem,
         * we need to update the model. If our hash contains @app, then
         * an in-place modification will be performed. Otherwise, we will
         * insert @item at a suitable position.
         */
        void updateModel( DFL::XDG::DesktopFile, QStandardItem * item );

        DFL::Inotify *fsw;
        DesktopParser *parser;
        QStringList mParsePaths;

        DFL::AppsList desktops;
        QHash<QString, QStandardItem *> items;

        /** Location of the Desktop Apps */
        const QStringList desktopPaths = {
            QDir::home().filePath( ".local/share/applications/" ),
            QDir::home().filePath( ".local/share/applications/wine/" ),
            QDir::home().filePath( ".local/share/games/" ),

            "/usr/local/share/applications/",
            "/usr/local/share/games/",

            "/usr/share/applications/",
            "/usr/share/games/"
        };

    protected:
        void mouseReleaseEvent( QMouseEvent *mEvent );
        void paintEvent( QPaintEvent *pEvent );
        void resizeEvent( QResizeEvent *rEvent );
        void keyPressEvent( QKeyEvent * );
        void focusInEvent( QFocusEvent * );

    Q_SIGNALS:
        void closeMenu();
        void menuReloaded();
        void updateCategories();

        void focusSearch();
};
