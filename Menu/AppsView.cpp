/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "AppsView.hpp"
#include "ViewsImpl.hpp"

#include <desq/Utils.hpp>

#include <DFL/DF5/Xdg.hpp>
#include <DFL/DF5/Inotify.hpp>

DesQ::Menu::ApplicationsView::ApplicationsView( QWidget *parent ) : QListView( parent ) {
    setViewMode( QListView::IconMode );
    setFlow( QListView::LeftToRight );
    setWrapping( true );
    setWordWrap( false );
    setTextElideMode( Qt::ElideRight );
    setResizeMode( QListView::Adjust );
    setMovement( QListView::Static );
    setIconSize( QSize( 64, 64 ) );
    setGridSize( QSize( 144, 108 ) );
    setFrameStyle( QFrame::NoFrame );
    setUniformItemSizes( true );
    viewport()->setAutoFillBackground( false );

    setItemDelegate( new AppsDelegate() );

    appsModel  = new QStandardItemModel( this );
    proxyModel = new QSortFilterProxyModel( this );
    proxyModel->setFilterCaseSensitivity( Qt::CaseInsensitive );
    proxyModel->setFilterRole( Qt::UserRole + 1 );

    proxyModel->setSourceModel( appsModel );
    setModel( proxyModel );

    parser = new DesktopParser();
    fsw    = new DFL::Inotify();

    connect(
        fsw, &DFL::Inotify::nodeCreated, [ = ] ( QString path ) mutable {
            QFileInfo info( path );

            qDebug() << "Node created:" << path;

            if ( info.isFile() and path.endsWith( ".desktop" ) ) {
                QString app( info.completeBaseName() );
                parser->addToParser( app );
            }

            else if ( info.isDir() ) {
                parseDesktops( { path } );
            }
        }
    );

    connect(
        fsw, &DFL::Inotify::nodeChanged, [ = ] ( QString path ) mutable {
            QFileInfo info( path );

            qDebug() << "Node changed:" << path;

            if ( info.isFile() and path.endsWith( ".desktop" ) ) {
                QString app( info.completeBaseName() );
                parser->addToParser( app );
            }

            else if ( info.isDir() ) {
                parseDesktops( { path } );
            }
        }
    );

    connect( fsw, &DFL::Inotify::nodeDeleted, this, &DesQ::Menu::ApplicationsView::removeDesktop );

    for ( QString path: desktopPaths ) {
        if ( QFile::exists( path ) ) {
            fsw->addWatch( path, DFL::Inotify::PathOnly );
        }
    }

    fsw->startWatch();

    connect( parser, &DesktopParser::appReady, this, &DesQ::Menu::ApplicationsView::updateModel );

    connect(
        this, &QListView::clicked, this, [ = ] ( QModelIndex item ) {
            QString desktopName = item.data( Qt::UserRole + 3 ).toString();
            DFL::XDG::DesktopFile appFile( desktopName );
            appFile.startApplication( {} );
            emit closeMenu();
        }
    );

    connect(
        this, &QListView::activated, this, [ = ] ( QModelIndex item ) {
            QString desktopName = item.data( Qt::UserRole + 3 ).toString();
            DFL::XDG::DesktopFile appFile( desktopName );
            appFile.startApplication( {} );
            emit closeMenu();
        }
    );

    std::function<void()> firstLoading = nullptr;
    firstLoading = [ this ] () {
                       parseDesktops( desktopPaths );
                       loadCategory( "All Applications" );
                   };

    QThread *loader = QThread::create( firstLoading );
    loader->start();
}


DesQ::Menu::ApplicationsView::~ApplicationsView() {
    disconnect();

    delete appsModel;
    delete proxyModel;
    delete fsw;
    delete parser;

    for ( QString app: items.keys() ) {
        QStandardItem *item = items.take( app );
        delete item;
    }
}


void DesQ::Menu::ApplicationsView::loadCategory( QString category ) {
    proxyModel->setFilterRole( Qt::UserRole + 2 );

    if ( category == "All Applications" ) {
        proxyModel->setFilterFixedString( "" );
    }

    else {
        proxyModel->setFilterFixedString( category );
    }

    setFocus();
}


void DesQ::Menu::ApplicationsView::setFilter( QString filter ) {
    proxyModel->setFilterRole( Qt::UserRole + 1 );
    proxyModel->setFilterWildcard( filter );
}


QStringList DesQ::Menu::ApplicationsView::availableCategories() {
    return mAvailableCategories;
}


void DesQ::Menu::ApplicationsView::parseDesktops( QStringList paths ) {
    /**
     * Irrespective of whether a desktop exists in the hash, we will be processing it.
     * We will simply use the base name of the desktop entry. That way the highest rank
     * desktop file will be chosen every time.
     * This approach has one disadvantage:
     * If multiple locations contain the same desktop file "some-app.desktop", then
     * "same-app" will be processed that many number of times.
     */

    quint64 start = 0, end = 0;

    start = QDateTime::currentDateTime().toMSecsSinceEpoch();
    QHash<QString, DFL::XDG::DesktopFile> desktops;

    for ( QString path: paths ) {
        QDir appDir( path );
        appDir.setFilter( QDir::Files );
        appDir.setNameFilters( { "*.desktop" } );

        Q_FOREACH ( QString entry, appDir.entryList() ) {
            QString app( QString( entry ).chopped( 8 ) );

            if ( not desktops.contains( app ) ) {
                parser->addToParser( app );
            }
        }
    }
    end = QDateTime::currentDateTime().toMSecsSinceEpoch();
    qDebug() << "Parsed desktops in" << (end - start) << "ms";
}


void DesQ::Menu::ApplicationsView::removeDesktop( QString path ) {
    qDebug() << "Removed" << path;
    QFileInfo info( path );

    if ( info.isFile() and path.endsWith( ".desktop" ) ) {
        QString app = info.completeBaseName();

        DFL::XDG::DesktopFile desktop( app );

        if ( desktop.isValid() and (desktop.type() == DFL::XDG::DesktopFile::Type::Application) ) {
            parser->addToParser( app );
        }

        else {
            QStandardItem *item = items.take( app );

            if ( item and item->row() > 0 ) {
                appsModel->removeRow( items[ app ]->row() );
                delete item;
            }
        }

        /** We're done here */
        return;
    }

    /**
     * If an entire folder was removed, we're not going to work too hard
     * This is on the user/sysadmin. Folders should not be removed for no reason.
     * Parse everything.
     */
    if ( info.isDir() ) {
        parseDesktops( desktopPaths );
    }
}


void DesQ::Menu::ApplicationsView::updateModel( DFL::XDG::DesktopFile desktop, QStandardItem *item ) {
    QString app( desktop.desktopName() );

    /** This desktop file exists */
    if ( items.contains( app ) ) {
        QStandardItem *oldItem = items[ app ];
        appsModel->setItem( oldItem->row(), item );
        items[ app ] = item;
    }

    else {
        desktops << desktop;
        std::sort(
            desktops.begin(), desktops.end(), [ ](const DFL::XDG::DesktopFile& lhs, const DFL::XDG::DesktopFile& rhs) {
                return lhs.name() < rhs.name();
            }
        );

        items[ app ] = item;

        int idx = desktops.indexOf( desktop );

        if ( desktops.count() - 1 == idx ) {
            appsModel->appendRow( item );
        }

        else {
            appsModel->insertRow( idx, item );
        }
    }

    /** Update the available categories */
    if ( not mAvailableCategories.contains( desktop.category() ) ) {
        mAvailableCategories << desktop.category();
        emit updateCategories();
    }

    qApp->processEvents();
}


void DesQ::Menu::ApplicationsView::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( not indexAt( mEvent->pos() ).isValid() ) {
        emit closeMenu();
    }

    QListView::mouseReleaseEvent( mEvent );
}


void DesQ::Menu::ApplicationsView::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( viewport() );

    painter.setPen( Qt::gray );
    painter.drawLine( geometry().topRight(), geometry().bottomRight() );
    painter.end();

    QListView::paintEvent( pEvent );
}


void DesQ::Menu::ApplicationsView::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    QSize vSize = viewport()->size();

    /* Items per row */
    float items = vSize.width() / (144 + spacing() );

    /* Minimum width of all items */
    float itemsWidth = items * 144;

    /* Empty space remaining */
    float empty = vSize.width() - itemsWidth;

    /* Extra space per item */
    float extra = (empty / items) - 5;

    /* New grid size */
    setGridSize( QSize( 144 + extra, 108 ) );
}


void DesQ::Menu::ApplicationsView::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Slash ) {
        emit focusSearch();
        kEvent->accept();

        return;
    }

    QListView::keyPressEvent( kEvent );
}


void DesQ::Menu::ApplicationsView::focusInEvent( QFocusEvent *fEvent ) {
    if ( not selectionModel()->hasSelection() ) {
        setCurrentIndex( model()->index( 0, 0 ) );
    }

    QListView::focusInEvent( fEvent );
}
