/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OtherViews.hpp"

DesQ::Menu::CategoryView::CategoryView( QWidget *parent ) : QListWidget( parent ) {
    setViewMode( QListView::ListMode );
    setIconSize( QSize( 32, 32 ) );
    setGridSize( QSize( 230, 36 ) );
    setFixedWidth( 250 );
    setFrameStyle( QFrame::NoFrame );
    viewport()->setAutoFillBackground( false );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    QStringList categories = {
        "All Applications", "Accessories", "Development", "Education",      "Games",    "Graphics",
        "Internet",         "Multimedia",  "Office",      "Science & Math", "Settings", "System", "Wine"
    };

    QStringList categoryIcons = {
        "preferences-system-windows", "applications-utilities",  "applications-development", "applications-education", "applications-games", "applications-graphics",
        "applications-internet",      "applications-multimedia", "applications-office",      "applications-science",   "preferences-other",  "applications-system",
        "wine"
    };

    for ( int i = 0; i < categories.count(); i++ ) {
        QListWidgetItem *item = new QListWidgetItem( QIcon::fromTheme( categoryIcons.value( i ) ), categories.at( i ) );
        item->setSizeHint( QSize( 230, 36 ) );
        item->setTextAlignment( Qt::AlignVCenter | Qt::AlignLeft );

        addItem( item );
    }

    connect(
        this, &QListWidget::itemClicked, this, [ = ] ( QListWidgetItem *item ) {
            emit loadCategory( item->text() );
        }
    );

    connect(
        this, &QListWidget::itemActivated, this, [ = ] ( QListWidgetItem *item ) {
            emit loadCategory( item->text() );
        }
    );

    setCurrentItem( item( 0 ) );
}


void DesQ::Menu::CategoryView::setCategories( QStringList categories ) {
    if ( not categories.contains( "All Applications" ) ) {
        categories << "All Applications";
    }

    for ( int r = 0; r < 13; r++ ) {
        QListWidgetItem *itm = item( r );

        if ( not categories.contains( itm->text() ) ) {
            itm->setHidden( true );
        }

        else {
            itm->setHidden( false );
        }
    }
}


void DesQ::Menu::CategoryView::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( not itemAt( mEvent->pos() ) ) {
        emit closeMenu();
    }

    QListWidget::mouseReleaseEvent( mEvent );
}


void DesQ::Menu::CategoryView::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Slash ) {
        emit focusSearch();
        kEvent->accept();

        return;
    }

    QListWidget::keyPressEvent( kEvent );
}


DesQ::Menu::FavoritesView::FavoritesView( QWidget *parent ) : QListWidget( parent ) {
    setViewMode( QListView::ListMode );
    setIconSize( QSize( 32, 32 ) );
    setGridSize( QSize( 230, 36 ) );
    setFixedWidth( 250 );
    setFrameStyle( QFrame::NoFrame );
    viewport()->setAutoFillBackground( false );

    QSettings favApps( "DesQ", "Menu" );

    Q_FOREACH ( QString desktop, favApps.value( "FavoriteApps" ).toStringList() ) {
        DesQ::DesktopFile appFile( desktop );
        QString           name   = appFile.name();
        QString           icoStr = appFile.icon();
        QIcon             icon   = QIcon::fromTheme( icoStr );

        if ( icon.isNull() ) {
            if ( QFile::exists( icoStr ) ) {
                icon = QIcon( icoStr );
            }

            else {
                icon = QIcon::fromTheme( "application-x-executable" );
            }
        }

        QListWidgetItem *item = new QListWidgetItem( icon, name );
        item->setData( Qt::UserRole + 1, appFile.desktopFileUrl() );
        item->setSizeHint( QSize( 230, 36 ) );
        item->setTextAlignment( Qt::AlignVCenter | Qt::AlignLeft );

        addItem( item );
    }

    connect(
        this, &QListWidget::itemClicked, this, [ = ] ( QListWidgetItem *item ) {
            QString desktopUrl = item->data( Qt::UserRole + 1 ).toString();
            DesQ::DesktopFile appFile( desktopUrl );
            appFile.startApplication( {} );

            emit closeMenu();
        }
    );

    connect(
        this, &QListWidget::itemActivated, this, [ = ] ( QListWidgetItem *item ) {
            QString desktopUrl = item->data( Qt::UserRole + 1 ).toString();
            DesQ::DesktopFile appFile( desktopUrl );
            appFile.startApplication( {} );

            emit closeMenu();
        }
    );
}


void DesQ::Menu::FavoritesView::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( not itemAt( mEvent->pos() ) ) {
        emit closeMenu();
    }

    QListWidget::mouseReleaseEvent( mEvent );
}


void DesQ::Menu::FavoritesView::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Slash ) {
        emit focusSearch();
        kEvent->accept();

        return;
    }

    QListWidget::keyPressEvent( kEvent );
}


void DesQ::Menu::FavoritesView::focusInEvent( QFocusEvent * ) {
    if ( not selectedItems().count() ) {
        setCurrentItem( item( 0 ) );
    }
}


void DesQ::Menu::FavoritesView::focusOutEvent( QFocusEvent * ) {
    selectionModel()->clearSelection();
}
