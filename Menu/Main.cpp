/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "UI.hpp"

#include <desq/Utils.hpp>

#include <DFL/DF5/Application.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Xdg.hpp>

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Menu.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Menu started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    DFL::Application app( argc, argv );

    app.setApplicationName( "Menu" );
    app.setOrganizationName( "DesQ" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-menu.desktop" );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    /* Process the CLI args */
    parser.process( app );

    qRegisterMetaType<DFL::XDG::DesktopFile>( "DesktopFile" );

    if ( app.lockApplication() ) {
        DesQ::Menu::UI *desqmenu = new DesQ::Menu::UI();

        QObject::connect(
            &app, &DFL::Application::messageFromClient, [ = ] ( QString message, int ) {
                if ( message == "Toggle" ) {
                    desqmenu->toggleVisible();
                }
            }
        );

        desqmenu->setup();

        return app.exec();
    }

    else {
        qDebug() << "DesQ Menu is running in the background. Bringing it up.";
        app.messageServer( "Toggle" );

        return 0;
    }

    return 0;
}
