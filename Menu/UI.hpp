/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Menu {
        class UI;
        class CategoryView;
        class ApplicationsView;
        class FavoritesView;
    }
}

class DesQ::Menu::UI : public QWidget {
    Q_OBJECT;

    public:
        UI();

        void setup();

    private:
        QLineEdit *searchEdit;                  // Search for applications
        QBasicTimer *delayTimer;                // Timer to delays search

        DesQ::Menu::CategoryView *catsView;     // Show the categories
        DesQ::Menu::ApplicationsView *appsView; // Show our applications
        DesQ::Menu::FavoritesView *favsView;    // Show the user favorites

        QLayout * setupLogoutButtons();

    public Q_SLOTS:
        void toggleVisible();

    protected:
        void mouseReleaseEvent( QMouseEvent *mEvent );
        void paintEvent( QPaintEvent *pEvent );
        void closeEvent( QCloseEvent *cEvent );

        void timerEvent( QTimerEvent * );

    Q_SIGNALS:
        void closed();
};
