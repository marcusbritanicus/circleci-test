# Shell

[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/EASXYBt6Qkj5T1qZccVda2/H1xEcoNNeyDF5SdNPwo6EV/tree/main.svg?style=shield&circle-token=3522fd54206a0f2598f399ec70417fa1ab7800b0)](https://dl.circleci.com/status-badge/redirect/circleci/EASXYBt6Qkj5T1qZccVda2/H1xEcoNNeyDF5SdNPwo6EV/tree/main)


The user interface for DesQ


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/Shell.git Shell`
- Enter the `Shell` folder
  * `cd Shell`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* QtWayland (qt5wayland5-dev, qtwayland5-private-dev)
* Qt SVG (libqt5svg5-dev)
* libsensors (libsensors-dev)
* libdbusmenu-qt5 (libdbusmenu-qt5-dev)
* wayland (libdbusmenu-qt5-dev)
* [libdesq](https://gitlab.com/DesQ/libdesq)
* [libdesqui](https://gitlab.com/DesQ/libdesqui)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::Xdg](https://gitlab.com/desktop-frameworks/xdg)
* [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Layouts](https://gitlab.com/desktop-frameworks/layouts)
* [DFL::WayQt](https://gitlab.com/desktop-frameworks/wayqt)
* [DFL::SNI](https://gitlab.com/desktop-frameworks/status-notifier)
* [DFL::Login1](https://gitlab.com/desktop-frameworks/login1)


### Known Bugs
* Pinning a window on all desktops does not work


### Upcoming
* Any other feature you request for... :)
