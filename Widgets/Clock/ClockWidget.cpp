/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include "ClockWidget.hpp"

ClockWidget::ClockWidget( QWidget *parent ) : QWidget( parent ) {
    mBatMgr = BatteryManager::instance();
    mBatMgr->refreshBatteries();

    QTimer *timer = new QTimer( this );

    connect( timer, SIGNAL(timeout()), this, SLOT(update()) );
    timer->start( 250 );

    setWindowTitle( tr( "Analog Clock" ) );
    resize( 200, 200 );
    setFixedSize( 200, 200 );

    prepareClockFace();
}


void ClockWidget::prepareClockFace() {
    clockFace = QImage( 200, 200, QImage::Format_ARGB32 );
    clockFace.fill( Qt::transparent );

    QPainter painter( &clockFace );

    painter.setRenderHint( QPainter::Antialiasing );

    QPalette pltt = palette();

    /* Clock center dot */
    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( pltt.color( QPalette::Window ) );
    painter.drawEllipse( QPointF( width() / 2, height() / 2 ), 3, 3 );
    painter.restore();

    painter.translate( width() / 2, height() / 2 );

    int side = qMin( width() - 20, height() - 20 );

    painter.scale( (side - 10) / 200.0, (side - 10) / 200.0 );

    /* Hour/minutes tics */
    painter.setPen( QPen( pltt.color( QPalette::WindowText ), 2.0, Qt::SolidLine, Qt::RoundCap ) );
    for ( int j = 0; j < 60; ++j ) {
        if ( (j % 5) == 0 ) {
            painter.drawLine( 88, 0, 96, 0 );
        }

        else {
            painter.drawLine( 92, 0, 96, 0 );
        }

        painter.rotate( 6.0 );
    }

    painter.end();
}


void ClockWidget::paintEvent( QPaintEvent * ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    QPalette pltt = palette();

    /* Size of the square */
    int side = qMin( width() - 20, height() - 20 );

    /* Draw battery circle track */
    painter.save();
    QColor clr = pltt.color( QPalette::Window );

    clr.setAlphaF( 0.1 );
    painter.setPen( QPen( clr, (side > 100 ? 10 : (side > 50 ? 6 : 4) ), Qt::SolidLine, Qt::RoundCap ) );
    painter.drawArc( QRect( (width() - side) / 2, (height() - side) / 2, side, side ), 90.0 * 16.0, -360.0 * 16.0 );
    painter.restore();

    // /* Battery percentage */
    if ( mBatMgr->hasBatteries() ) {
        Battery mainBattery = mBatMgr->batteries().at( 0 );
        int     battpc      = mainBattery.value( "Percentage" ).toInt();

        QColor battColor = (battpc <= 10 ? Qt::darkRed : (battpc <= 50 ? Qt::darkYellow : Qt::white) );

        painter.save();
        painter.setPen( QPen( battColor, (side > 100 ? 8 : (side > 50 ? 4 : 2) ), Qt::SolidLine, Qt::RoundCap ) );
        painter.drawArc( QRect( (width() - side) / 2, (height() - side) / 2, side, side ), 90.0 * 16.0, -360.0 * 16.0 * battpc / 100.0 );
        painter.restore();
    }

    /* Get the current time */
    QDateTime datetime = QDateTime::currentDateTime();

    QDate date = datetime.date();
    QTime time = datetime.time();

    painter.drawImage( rect(), clockFace );

    painter.translate( width() / 2, height() / 2 );
    painter.scale( (side - 10) / 200.0, (side - 10) / 200.0 );

    /* Hour Hand */
    painter.save();
    painter.rotate( 30.0 * ( (time.hour() + time.minute() / 60.0) ) );
    painter.setPen( QPen( pltt.color( QPalette::WindowText ), 5.0, Qt::SolidLine, Qt::RoundCap ) );
    painter.drawLine( QLineF( QPointF( 0, -8 ), QPointF( 0, -40 ) ) );
    painter.restore();

    /* Minute Hand */
    painter.save();
    painter.rotate( 6.0 * (time.minute() + time.second() / 60.0) );
    painter.setPen( QPen( pltt.color( QPalette::WindowText ), 4.0, Qt::SolidLine, Qt::RoundCap ) );
    painter.drawLine( QLineF( QPointF( 0, -8 ), QPointF( 0, -70 ) ) );
    painter.restore();

    /* Second Hand */
    painter.save();
    painter.rotate( 6.0 * (time.second() + time.msec() / 1000.0) );
    painter.setPen( QPen( Qt::red, 2.0, Qt::SolidLine, Qt::RoundCap ) );
    painter.drawLine( QLineF( QPointF( 0, -8 ), QPointF( 0, -80 ) ) );
    painter.restore();

    /* Date */
    painter.resetTransform();
    painter.save();
    QFont f( font() );

    painter.setFont( QFont( f.family(), f.pointSize() + 1, QFont::Bold ) );
    painter.setPen( pltt.color( QPalette::Window ) );
    painter.drawText( QRectF( 1, height() * 0.70 + 1, width(), height() ), Qt::AlignHCenter | Qt::AlignTop, date.toString( "MMM dd, yyyy" ) );
    painter.setPen( pltt.color( QPalette::WindowText ) );
    painter.drawText( QRectF( 0, height() * 0.70 + 0, width(), height() ), Qt::AlignHCenter | Qt::AlignTop, date.toString( "MMM dd, yyyy" ) );
    painter.restore();

    /* End the painting */
    painter.end();
}


DigitalClock::DigitalClock( QWidget *parent ) : QWidget( parent ) {
    QDateTime dt   = QDateTime::currentDateTime();
    int       size = font().pointSize() * 16.0 / 12.0 * 2.0;

    time = new QLCDNumber( this );
    time->setDigitCount( 5 );
    time->setSegmentStyle( QLCDNumber::Flat );
    time->setFrameStyle( QLCDNumber::NoFrame );
    time->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding ) );
    time->setMinimumHeight( size * 2 );
    time->display( dt.toString( "hh:mm" ) );

    secs = new QLCDNumber( this );
    secs->setDigitCount( 2 );
    secs->setSegmentStyle( QLCDNumber::Flat );
    secs->setFrameStyle( QLCDNumber::NoFrame );
    secs->setFixedSize( QSize( size, size ) );
    secs->display( dt.toString( "ss" ) );

    ampm = new QLabel();
    ampm->setAlignment( Qt::AlignCenter );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );

    date = new QLabel();
    date->setAlignment( Qt::AlignCenter );
    date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );

    alrm = new QLabel();
    alrm->setAlignment( Qt::AlignCenter );
    alrm->setText( QString::fromUtf8( "\xF0\x9F\x94\x94" ) );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( time, 0, 0, 2, 1 );
    lyt->addWidget( secs, 0, 1, Qt::AlignCenter );
    lyt->addWidget( ampm, 1, 1, Qt::AlignCenter );
    lyt->addWidget( date, 2, 0, Qt::AlignCenter );
    lyt->addWidget( alrm, 2, 1, Qt::AlignCenter );
    setLayout( lyt );

    setFixedHeight( size * 3.5 );

    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();

    shadow->setOffset( 1, 1 );
    shadow->setColor( palette().color( QPalette::Window ) );
    setGraphicsEffect( shadow );

    timer = new QBasicTimer();
    timer->start( 1000, this );
}


void DigitalClock::timerEvent( QTimerEvent *event ) {
    if ( event->timerId() == timer->timerId() ) {
        QDateTime dt = QDateTime::currentDateTime();
        time->display( dt.toString( "hh:mm" ) );
        secs->display( dt.toString( "ss" ) );
        ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );
        date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );
    }

    QWidget::timerEvent( event );
}
