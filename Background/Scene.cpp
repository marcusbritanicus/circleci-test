/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Scene.hpp"

#include <desq/Utils.hpp>
#include <desq/Xdg.hpp>
#include <DFL/DF5/Settings.hpp>
#include <desq/desq-config.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>

DesQ::Scene::UI::UI( QScreen *scr, QString imageLoc ) : QWidget() {
    mScreen = scr;

    /* We need our desktop to fill the whole physical screen */
    setFixedSize( mScreen->size() );
    connect( mScreen, &QScreen::geometryChanged, this, &DesQ::Scene::UI::resizeDesktop );

    /* If the display manager is X11, we need to let it know that we are a desktop */
    setAttribute( Qt::WA_X11NetWmWindowTypeDesktop );

    /* Title */
    setWindowTitle( "DesQ Scene" );

    /* No frame, and stay at bottom */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

    /* Set the windowFlags */
    setWindowFlags( wFlags );

    /* Background image */
    bgImagePath = imageLoc;

    /* Background position */
    if ( shellSett->hasKey( "Output:" + mScreen->name() + "/BackgroundPosition" ) ) {
        wallpaperPos = ( int )shellSett->value( "Output:" + mScreen->name() + "/BackgroundPosition" );
    }

    else {
        wallpaperPos = ( int )shellSett->value( "BackgroundPosition" );
    }

    /* Prepare the background */
    prepareBackgroundImage();
}


DesQ::Scene::UI::~UI() {
    mScreen->disconnect();
    delete mScreen;
}


void DesQ::Scene::UI::update( QString what ) {
    if ( what == "opacity" ) {
        QVariantAnimation *anim = createAnimator();
        anim->start();

        return;
    }

    /** Update the bg image from settings */
    if ( what == "Background" ) {
        if ( shellSett->hasKey( "Output:" + mScreen->name() + "/Background" ) ) {
            bgImagePath = ( QString )shellSett->value( "Output:" + mScreen->name() + "/Background" );
        }

        else {
            bgImagePath = ( QString )shellSett->value( "Background" );
        }
    }

    /** Update the bg image from settings */
    else if ( what == "BackgroundPosition" ) {
        if ( shellSett->hasKey( "Output:" + mScreen->name() + "/BackgroundPosition" ) ) {
            wallpaperPos = ( int )shellSett->value( "Output:" + mScreen->name() + "/BackgroundPosition" );
        }

        else {
            wallpaperPos = ( int )shellSett->value( "BackgroundPosition" );
        }
    }

    else {
        bgImagePath = what;
    }

    prepareBackgroundImage();
}


void DesQ::Scene::UI::cacheImage() {
    /**
     * We'll be saving the image as ~/.cache/DesQ/background.xxx
     * Additionally, we'll write an info file that contains the image path,
     * wallpaper position, hash, etc. This info file will be used
     * by isCacheFresh().
     */

    QDir cache( DesQ::XDG::xdgCacheHome() + "/DesQ/" );

    QFile imgFile( bgImagePath );

    imgFile.open( QFile::ReadOnly );

    QCryptographicHash hash( QCryptographicHash::Sha512 );

    hash.addData( imgFile.readAll() );
    imgFile.close();

    QSettings sett( cache.filePath( "background.ini" ), QSettings::IniFormat );

    sett.beginGroup( "Output:" + mScreen->name() );
    sett.setValue( "File",     bgImagePath );
    sett.setValue( "Position", wallpaperPos );
    sett.setValue( "Hash",     hash.result() );
    sett.endGroup();

    sett.sync();

    desktopImage.save( cache.filePath( "background-" + mScreen->name() + ".jpg" ) );
}


bool DesQ::Scene::UI::isCacheFresh() {
    QDir      cache( DesQ::XDG::xdgCacheHome() + "/DesQ/" );
    QSettings sett( cache.filePath( "background.ini" ), QSettings::IniFormat );

    if ( not QFile::exists( cache.filePath( "background-" + mScreen->name() + ".jpg" ) ) ) {
        return false;
    }

    sett.beginGroup( "Output:" + mScreen->name() );

    if ( sett.value( "File" ).toString() != bgImagePath ) {
        sett.endGroup();
        return false;
    }

    if ( sett.value( "Position" ).toInt() != wallpaperPos ) {
        sett.endGroup();
        return false;
    }

    QFile              imgFile( bgImagePath );
    QCryptographicHash hash( QCryptographicHash::Sha512 );

    imgFile.open( QFile::ReadOnly );
    hash.addData( imgFile.readAll() );
    imgFile.close();

    if ( sett.value( "Hash" ).toByteArray() != hash.result() ) {
        sett.endGroup();
        return false;
    }

    sett.endGroup();
    cachedImagePath = cache.filePath( "background-" + mScreen->name() + ".jpg" );

    return true;
}


void DesQ::Scene::UI::prepareBackgroundImage() {
    /* If the BG path was not specified by the user, use saved path */
    if ( bgImagePath.isEmpty() ) {
        if ( shellSett->hasKey( "Output:" + mScreen->name() + "/Background" ) ) {
            bgImagePath = ( QString )shellSett->value( "Output:" + mScreen->name() + "/Background" );
        }

        else {
            bgImagePath = ( QString )shellSett->value( "Background" );
        }
    }

    /** If the user does not want a background image */
    if ( bgImagePath == "None" ) {
        return;
    }

    /** User has specified a solid color? */
    if ( bgImagePath.startsWith( "#" ) ) {
        QColor clr( bgImagePath );
        desktopImage.fill( clr );

        repaint();

        return;
    }

    /* If the specified path/saved path does not exist, use the default value */
    if ( not QFile::exists( bgImagePath ) ) {
        bgImagePath = SharePath "resources/default.jpg";
    }

    if ( isCacheFresh() ) {
        desktopImage = QImage( cachedImagePath );

        if ( desktopImage.size() == mScreen->size() ) {
            mOpacity = 0.0;
            update( "opacity" );
            return;
        }

        else {
            qDebug() << cachedImagePath;
            qDebug() << desktopImage.size() << mScreen->size();
        }
    }

    QSize drawSize = mScreen->geometry().size();

    QImageReader ir( bgImagePath );
    QSize        pixSize = ir.size();

    mPicOffset = QPoint();

    switch ( wallpaperPos ) {
        // Centered: Show the full wallpaper at the center
        case 0: {
            pixSize    = pixSize.scaled( drawSize, Qt::KeepAspectRatio );
            mPicOffset = { abs( pixSize.width() - drawSize.width() ) / 2, abs( pixSize.height() - drawSize.height() ) / 2 };

            break;
        }

        // Stretched: Ignore aspect ratio
        case 1: {
            pixSize = pixSize.scaled( drawSize, Qt::IgnoreAspectRatio );
            break;
        }

        // Scaled: Qt::KeepAspectRatioByExpanding
        case 2: {
            pixSize = pixSize.scaled( drawSize, Qt::KeepAspectRatioByExpanding );
            break;
        }
    }

    ir.setScaledSize( pixSize );

    /* Currently, the default setting: Scaled and cropped */
    QImage tmpImg = ir.read();

    desktopImage = tmpImg.copy( QRect( QPoint( 0, 0 ), mScreen->size() ) );

    /* Cache the scaled image so that it can be used later on */
    cacheImage();

    /** Set the opacity to zero */
    mOpacity = 0.0;
    update( "opacity" );
}


void DesQ::Scene::UI::resizeDesktop() {
    qDebug() << "Screen resized";
    qDebug() << mScreen->size();

    /* Reset the background */
    prepareBackgroundImage();

    update( "opacity" );
}


void DesQ::Scene::UI::paintEvent( QPaintEvent *pEvent ) {
    /** If we have a valid desktop image */
    if ( not desktopImage.isNull() ) {
        QPainter painter( this );

        painter.setCompositionMode( QPainter::CompositionMode_SourceOver );
        painter.setOpacity( mOpacity );

        painter.setPen( Qt::NoPen );
        painter.setPen( Qt::NoBrush );

        painter.setRenderHints( QPainter::Antialiasing );

        painter.drawImage( geometry(), desktopImage );

        painter.end();
    }

    /** Done with the painting */
    pEvent->accept();
}


QVariantAnimation * DesQ::Scene::UI::createAnimator() {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    anim->setStartValue( 0.0 );
    anim->setEndValue( 1.0 );
    anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuad ) );

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) mutable {
            mOpacity = val.toDouble();
            QWidget::update();
        }
    );

    return anim;
}
