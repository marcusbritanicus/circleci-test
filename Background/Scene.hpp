/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>
#include <QtGui>
#include <QtCore>
#include <QtSvg>

namespace DesQ {
    namespace Scene {
        class UI;
    }
}


class DesQ::Scene::UI : public QWidget {
    Q_OBJECT

    public:
        UI( QScreen *scr, QString img = QString() );
        ~UI();

        /** Update the background/position from the settings or the image itself */
        void update( QString );

    private:
        /** Store the image with the reduced size for faster loading */
        void cacheImage();

        /** Is the cached image fresh? */
        bool isCacheFresh();

        /** Load the image and scale it to needed resolution, size, etc. */
        void prepareBackgroundImage();

        /** Screen-size changed: reset the wallpaper */
        void resizeDesktop();

        QString bgImagePath;
        QString cachedImagePath;
        QImage desktopImage;

        qreal mOpacity = 0.0;

        int wallpaperPos = 0;
        QPoint mPicOffset;

        QScreen *mScreen;

        /**
         * Return an animator which will animate a layer surface when started.
         * @surf - Layer surface to be moved
         * @pw   - Instance of paperwidget (used to calculate the margin positions)
         * @show - If true, the widget is being shown, otherwise hidden
         */
        QVariantAnimation *createAnimator();

    protected:
        void paintEvent( QPaintEvent *pEvent );
};
