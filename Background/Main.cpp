/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Manager.hpp"

// LibDesQ Headers
#include <desq/Utils.hpp>
#include <desq/desq-config.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <DFL/DF5/Application.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Xdg.hpp>

DFL::Settings *shellSett;
WQt::Registry *wlRegistry;

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Scene.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Scene started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    DFL::Application app( argc, argv );

    app.setApplicationName( "Scene" );
    app.setOrganizationName( "DesQ" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-scene.desktop" );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    /* Path to the background file */
    parser.addOption( { { "b", "background" }, "The path to the background file.", "image file" } );

    /* Path to the background file */
    parser.addOption( { { "o", "output" }, "Output on which the background is to be changed", "output" } );

    /* Save this? */
    parser.addOption( { { "s", "save" }, "Save the image file in settings for the next start." } );

    /* Process the CLI args */
    parser.process( app );

    if ( app.lockApplication() ) {
        /* Init DFL::Settings object */
        shellSett = new DFL::Settings( "DesQ", "Shell", ConfigPath );

        wlRegistry = new WQt::Registry( WQt::Wayland::display() );

        wlRegistry->setup();

        qDebug() << "Starting desq-scene";
        QString bgValue( parser.value( "background" ) );

        DesQ::Scene::Manager *bgMgr = new DesQ::Scene::Manager( bgValue );

        QObject::connect( &app, &DFL::Application::messageFromClient, bgMgr, &DesQ::Scene::Manager::handleMessages );

        return app.exec();
    }

    else {
        if ( parser.isSet( "background" ) ) {
            QString msg( "background\n" );

            /** Save this permanently */
            if ( parser.isSet( "save" ) ) {
                msg = "set-" + msg;
            }

            /** Use this background only on a particular output */
            if ( parser.isSet( "output" ) ) {
                msg += parser.value( "output" ) + "\n";
            }

            /** Name of the background file */
            msg += parser.value( "background" );
            app.messageServer( msg );
        }

        if ( parser.isSet( "save" ) and not parser.isSet( "background" ) ) {
            qDebug() << "-s/--save cannot be used alone.";
            parser.showHelp();
        }

        if ( parser.isSet( "output" ) and not parser.isSet( "background" ) ) {
            qDebug() << "-o/--output cannot be used alone.";
            parser.showHelp();
        }

        return 0;
    }

    return 0;
}
