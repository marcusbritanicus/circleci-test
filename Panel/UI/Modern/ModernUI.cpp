/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include "Global.hpp"
#include "ModernUI.hpp"
#include "StyleSheet.hpp"

#include "Widgets.hpp"
#include "Clock.hpp"
#include "PagerWidget.hpp"
#include "SNHost.hpp"
#include "TaskButton.hpp"

#include <desq/Utils.hpp>

#include <desq/desq-config.h>
#include <desqui/ShellPlugin.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

DesQ::Panel::ModernUI::ModernUI( QScreen *scrn ) : DesQ::Panel::AbstractUI() {
    mScreen = scrn;

    /* No frame, and stay at bottom */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

    panelHeight = ( int )shellSett->value( "Panel/Height" );

    setAttribute( Qt::WA_TranslucentBackground );
    setContentsMargins( QMargins() );

    /* Set the windowFlags */
    setWindowFlags( wFlags );

    setWindowTitle( tr( "DesQ Panel" ) );

    buildPanel();
    autoResize();

    connect( qApp->primaryScreen(), &QScreen::geometryChanged, this, &DesQ::Panel::ModernUI::autoResize );
}


DesQ::Panel::ModernUI::~ModernUI() {
    delete tasks;
    delete pager;
    delete clock;
    delete tray;
    delete powerBtn;
    delete widgetLyt;
    delete mScreen;
}


void DesQ::Panel::ModernUI::autoResize() {
    // Span the entire width of the screen
    QSize scrSize = mScreen->size();

    setFixedSize( scrSize.width(), panelHeight );
}


void DesQ::Panel::ModernUI::buildPanel() {
    tasks = new DesQ::Panel::TaskButton( this );
    connect( tasks, &DesQ::Panel::TaskButton::showViewsWidget, this, &DesQ::Panel::ModernUI::showWidget );

    pager = new PagerWidget( this );
    clock = new ClockLabel( this );

    tray = new StatusNotifierHost( StatusNotifierHost::Horizontal, this );
    tray->setFixedHeight( panelHeight );
    tray->setButtonSize( panelHeight );

    powerBtn = new QToolButton();
    powerBtn->setIcon( QIcon::fromTheme( "system-shutdown" ) );
    powerBtn->setAutoRaise( true );
    powerBtn->setFixedSize( QSize( panelHeight, panelHeight ) );
    powerBtn->setIconSize( QSize( panelHeight - 4, panelHeight - 4 ) );

    widgetLyt = new QHBoxLayout();
    widgetLyt->setSpacing( 0 );
    widgetLyt->setContentsMargins( QMargins() );

    widgetLyt->addWidget( tasks );
    widgetLyt->addWidget( new QLabel( " | " ) );
    widgetLyt->addWidget( pager );

    widgetLyt->addStretch();

    widgetLyt->addWidget( tray );
    widgetLyt->addWidget( clock );
    widgetLyt->addWidget( new QLabel( " | " ) );
    widgetLyt->addWidget( powerBtn );

    QWidget *base = new QWidget( this );

    base->setLayout( widgetLyt );

    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->setContentsMargins( QMargins() );
    baseLyt->addWidget( base );

    setLayout( baseLyt );
}


void DesQ::Panel::ModernUI::showWidget( QWidget *views ) {
    views->show();

    WQt::LayerShell::LayerType lyr = WQt::LayerShell::Top;

    wl_output         *output = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );
    WQt::LayerSurface *cls    = wlRegistry->layerShell()->getLayerSurface( views->windowHandle(), output, lyr, "panel" );

    /** Stuck to the Top/Bottom (for now). We need to implement settings */
    cls->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Left );

    /** Size of our surface */
    cls->setSurfaceSize( views->size() );

    /** Leave a space of 10 px on the left and 10 px from the panel. */
    cls->setMargins( QMargins( 10, panelHeight + 10, 0, 0 ) );

    /** We may need keyboard interaction (in future). Nothing needed now */
    cls->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );

    /** Commit to our choices */
    cls->apply();
}


void DesQ::Panel::ModernUI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();
    painter.setBrush( QColor( 0, 0, 0, 180 ) );
    painter.setPen( Qt::NoPen );
    painter.drawRect( 0, 0, width(), height() );
    painter.restore();

    painter.end();

    QWidget::paintEvent( pEvent );
}
