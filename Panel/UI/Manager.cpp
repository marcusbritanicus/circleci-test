/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Manager.hpp"
#include "ModernUI.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFL/DF5/Application.hpp>

DesQ::Panel::Manager::Manager() {
    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn );

        QVariantAnimation *showAnim = createAnimator( mSurfaces[ scrn->name() ], mInstances[ scrn->name() ], true );
        showAnim->start();
        mInstanceStates[ scrn->name() ] = true;
    }
}


DesQ::Panel::Manager::~Manager() {
}


void DesQ::Panel::Manager::handleMessages( QString mesg, int fd ) {
    if ( mesg.startsWith( "reload" ) ) {
    }

    else if ( mesg.startsWith( "raise" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( (parts.count() >= 2) ) {
            raiseInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "lower" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( (parts.count() >= 2) ) {
            lowerInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "toggle" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( mInstanceStates[ parts[ 1 ] ] ) {
            hideLayerSurface( parts[ 1 ] );
            qApp->messageClient( "hidden", fd );
        }

        else {
            showLayerSurface( parts[ 1 ] );
            qApp->messageClient( "shown", fd );
        }
    }

    else {
        qDebug() << "Invalid command\n";
        return;
    }
}


void DesQ::Panel::Manager::createInstance( QScreen *scrn ) {
    DesQ::Panel::AbstractUI *ui = new DesQ::Panel::ModernUI( scrn );

    ui->show();

    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType lyr = WQt::LayerShell::Bottom;
        switch ( (int)shellSett->value( "Panel/AutoHide" ) ) {
            case DesQ::Panel::AbstractUI::Never: {
                lyr = WQt::LayerShell::Top;
                break;
            }

            case DesQ::Panel::AbstractUI::Always: {
                lyr = WQt::LayerShell::Bottom;
                break;
            }

            case DesQ::Panel::AbstractUI::WindowsCanCover: {
                lyr = WQt::LayerShell::Bottom;
                break;
            }
        }

        /** wl_output corresponding to @screen */
        wl_output         *output = WQt::Utils::wlOutputFromQScreen( scrn );
        WQt::LayerSurface *cls    = wlRegistry->layerShell()->getLayerSurface( ui->windowHandle(), output, lyr, "desq-panel" );

        /** Stuck to the Top/Bottom (for now). We need to implement settings */
        switch ( (int)shellSett->value( "Panel/Position" ) ) {
            case DesQ::Panel::AbstractUI::Left: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }

            case DesQ::Panel::AbstractUI::Top: {
                cls->setAnchors( WQt::LayerSurface::Top );
                break;
            }

            case DesQ::Panel::AbstractUI::Right: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }

            case DesQ::Panel::AbstractUI::Bottom: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }
        }

        /** Size of our surface */
        cls->setSurfaceSize( ui->size() );

        /** No margins (for now). We can implment DesQ Dock like settings for floating panel that hides */
        cls->setMargins( QMargins( 0, -ui->height(), 0, 0 ) );

        /** Do not move this surface at any cost. */
        cls->setExclusiveZone( ui->height() );

        /** We may need keyboard interaction (in future). Nothing needed now */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Commit to our choices */
        cls->apply();

        mInstances[ scrn->name() ]      = ui;
        mSurfaces[ scrn->name() ]       = cls;
        mSurfaceStates[ scrn->name() ]  = lyr;
        mInstanceStates[ scrn->name() ] = true;
    }

    else {
        ui->close();
        delete ui;
    }
}


void DesQ::Panel::Manager::destroyInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) ) {
        WQt::LayerSurface *surf = mSurfaces.take( opName );
        delete surf;
    }

    if ( mSurfaceStates.contains( opName ) ) {
        mSurfaceStates.remove( opName );
    }

    if ( mInstances.contains( opName ) ) {
        DesQ::Panel::AbstractUI *w = mInstances.take( opName );
        w->close();
        delete w;
    }

    if ( mInstanceStates.contains( opName ) ) {
        mInstanceStates.remove( opName );
    }
}


void DesQ::Panel::Manager::raiseInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Top ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Top;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::lowerInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Bottom ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Bottom;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Bottom );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::showLayerSurface( QString opName ) {
    if ( not mInstances.contains( opName ) ) {
        return;
    }

    mSurfaceStates[ opName ] = WQt::LayerShell::Top;

    mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
    mSurfaces[ opName ]->apply();

    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    showAnim->start();

    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::hideLayerSurface( QString opName ) {
    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );

    hideAnim->start();

    mInstanceStates[ opName ] = false;
}


QVariantAnimation * DesQ::Panel::Manager::createAnimator( WQt::LayerSurface *surf, DesQ::Panel::AbstractUI *ui, bool show ) {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    if ( show ) {
        anim->setStartValue( -ui->height() );
        anim->setEndValue( 0 );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::OutCubic ) );
    }

    else {
        anim->setStartValue( 0 );
        anim->setEndValue( -ui->width() );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuart ) );
    }

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
            surf->setMargins( QMargins( 0, val.toInt(), 0, 0 ) );
            surf->apply();

            qApp->processEvents();
        }
    );

    return anim;
}
