/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "ModernUI.hpp"
#include "Manager.hpp"

#include <signal.h>
#include <desq/desq-config.h>

#include <DFL/DF5/Xdg.hpp>
#include <DFL/DF5/Utils.hpp>
#include <DFL/DF5/Application.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WindowManager.hpp>

DFL::Settings *shellSett  = nullptr;
WQt::Registry *wlRegistry = nullptr;

QDBusInterface *compositor = nullptr;
QDBusInterface *workspaces = nullptr;
QDBusInterface *output     = nullptr;
QDBusInterface *views      = nullptr;

int main( int argc, char *argv[] ) {
#if (QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 ) )
        QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    QDir cache( DFL::XDG::xdgCacheHome() );
    DFL::log = fopen( cache.filePath( "DesQ/Panel.log" ).toLocal8Bit().data(), "a" );

    DFL::SHOW_INFO_ON_CONSOLE     = true;
    DFL::SHOW_DEBUG_ON_CONSOLE    = true;
    DFL::SHOW_WARNING_ON_CONSOLE  = false;
    DFL::SHOW_CRITICAL_ON_CONSOLE = true;
    DFL::SHOW_FATAL_ON_CONSOLE    = true;

    qInstallMessageHandler( DFL::Logger );

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Panel" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-panel.desktop" );

    app->interceptSignal( SIGSEGV, true );
    app->interceptSignal( SIGABRT, true );
    app->interceptSignal( SIGTERM, true );
    app->interceptSignal( SIGQUIT, true );
    app->interceptSignal( SIGINT,  true );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "s", "raise" }, "Raise the cask to the top" } );
    parser.addOption( { { "l", "lower" }, "Lower the cask to the bottom" } );
    parser.addOption( { { "t", "toggle" }, "Show/hide the cask instance" } );
    parser.addOption( { { "o", "output" }, "Output on which raise/lower/toggle is done.", "output" } );

    parser.process( *app );

    /** If the panel is already running, bug out */
    if ( app->isRunning() ) {
        qDebug() << "Panel is already running";

        if ( parser.isSet( "raise" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Panel instance is to be raised.";
                return 1;
            }

            app->messageServer( "raise\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "lower" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Panel instance is to be lowered.";
                return 1;
            }

            app->messageServer( "lower\n" + parser.value( "output" ) );
        }

        return 0;
    }

    if ( not app->lockApplication() ) {
        qDebug() << "Unable to lock this instance. Aborting...";
        return 1;
    }

    /** Prepare the DesQ Settings */
    shellSett = new DFL::Settings( "DesQ", "Shell", ConfigPath );

    /** Prepare the DBus Interfaces */
    compositor = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor", QDBusConnection::sessionBus() );
    output     = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.output", QDBusConnection::sessionBus() );
    workspaces = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.workspace", QDBusConnection::sessionBus() );
    views      = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.views", QDBusConnection::sessionBus() );

    /** Wayland Registry */
    wlRegistry = new WQt::Registry( WQt::Wayland::display() );
    wlRegistry->setup();

    DesQ::Panel::Manager *panelMgr = new DesQ::Panel::Manager();

    QObject::connect( app, &DFL::Application::messageFromClient, panelMgr, &DesQ::Panel::Manager::handleMessages );

    QObject::connect(
        app, &QApplication::screenAdded, [ panelMgr ] ( QScreen *screen ) {
            panelMgr->createInstance( screen );
        }
    );

    // /** Minimize rectangle */
    // WQt::WindowManager *mgr  = wlRegistry->windowManager();
    // QObject::connect(
    //     mgr, &WQt::WindowManager::newTopLevelHandle, [ = ]( WQt::WindowHandle *hndl ) {
    //         hndl->setMinimizeRect( surf, QRect( 0, 0, 270, ( int )shellSett->value( "Panel/Height" ) ) );
    //     }
    // );
    //
    // /** Start monitoring */
    // mgr->setup();

    return app->exec();
}
