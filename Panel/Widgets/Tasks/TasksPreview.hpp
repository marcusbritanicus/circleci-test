/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtDBus>
#include <QtWidgets>

#include <wayqt/WindowManager.hpp>

#include "dbustypes.hpp"

namespace DesQ {
    namespace Panel {
        class Task;
        class TasksPreview;
    }
}

class DesQ::Panel::Task : public QWidget {
    Q_OBJECT;

    public:
        Task( uint viewsIds );
        ~Task();

    private:
        uint mViewId;

        QImage mThumb;

        QPixmap mIcon;
        QString mTitle;

        bool mPressed = false;
        bool mActive  = false;
        bool mHover   = false;

    protected:
        void enterEvent( QEvent * );
        void leaveEvent( QEvent * );

        void mousePressEvent( QMouseEvent * );
        void mouseReleaseEvent( QMouseEvent * );

        void paintEvent( QPaintEvent * );

    Q_SIGNALS:
        void activate( uint );
};


class DesQ::Panel::TasksPreview : public QWidget {
    Q_OBJECT;

    public:
        TasksPreview( QUIntList viewsIds, uint activeId );
        ~TasksPreview();

    private:
        QList<int> taskIds;
        QList<Task *> tasks;

        int curIdx = -1;

    protected:
        void keyReleaseEvent( QKeyEvent *kEvent );
        void paintEvent( QPaintEvent *pEvent );

    Q_SIGNALS:
        void activate( uint );
};
