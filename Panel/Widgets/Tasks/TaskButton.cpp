/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "MiscTools.hpp"
#include "TaskButton.hpp"
#include "TasksPreview.hpp"

#include <desq/Utils.hpp>

DesQ::Panel::TaskButton::TaskButton( QWidget *parent ) : QWidget( parent ) {
    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceGrid>( "WorkSpaceGrid" );
    qDBusRegisterMetaType<WorkSpaceGrid>();

    longPressTimer = new QBasicTimer();

    buildUI();

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "OutputChanged", "u", this,
        SLOT( handleOutputChanged( uint ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "OutputWorkspaceChanged", "u(ii)", this,
        SLOT( handleWorkspaceChanged( uint, WorkSpace ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewFocusChanged", "u", this,
        SLOT( setActive( uint ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewAdded", "u", this,
        SLOT( addTask( uint ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewRemoved", "u", this,
        SLOT( removeTask( uint ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewMinimizedChanged", "ub", this,
        SLOT( handleMinimizeChanged( uint, bool ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewMaximizedChanged", "ub", this,
        SLOT( handleMaximizeChanged( uint, bool ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewTitleChanged", "us", this,
        SLOT( updateButtonTitle( uint, QString ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewAppIdChanged", "us", this,
        SLOT( updateButtonIcon( uint, QString ) )
    );

    QDBusReply<uint> replyOP = output->call( "QueryActiveOutput" );

    if ( replyOP.isValid() ) {
        handleOutputChanged( replyOP.value() );
    }
}


void DesQ::Panel::TaskButton::buildUI() {
    int panelHeight = shellSett->value( "Panel/Height" );

    mIconSize = panelHeight - 12;

    setFixedSize( 270, panelHeight );

    iconLbl = new QLabel();
    iconLbl->setFixedSize( panelHeight, panelHeight );
    iconLbl->setAlignment( Qt::AlignCenter );
    iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( mIconSize ) );

    textLbl = new QLabel();
    textLbl->setFixedHeight( panelHeight );
    textLbl->setMinimumWidth( 270 - panelHeight * 4 );
    textLbl->setText( "Applications" );

    minBtn = new QToolButton();
    minBtn->setIcon( QIcon::fromTheme( "window-minimize" ) );
    minBtn->setFixedSize( 24, 24 );
    minBtn->setAutoRaise( true );
    minBtn->setIconSize( QSize( 16, 16 ) );
    minBtn->hide();
    connect( minBtn, &QToolButton::clicked, this, &DesQ::Panel::TaskButton::minimizeCurrentView );

    maxBtn = new QToolButton();
    maxBtn->setIcon( QIcon::fromTheme( "window-maximize" ) );
    maxBtn->setFixedSize( 24, 24 );
    maxBtn->setAutoRaise( true );
    maxBtn->setIconSize( QSize( 16, 16 ) );
    maxBtn->hide();
    connect( maxBtn, &QToolButton::clicked, this, &DesQ::Panel::TaskButton::maximizeCurrentView );

    closeBtn = new QToolButton();
    closeBtn->setIcon( QIcon::fromTheme( "window-close" ) );
    closeBtn->setFixedSize( 24, 24 );
    closeBtn->setAutoRaise( true );
    closeBtn->setIconSize( QSize( 16, 16 ) );
    closeBtn->hide();
    connect( closeBtn, &QToolButton::clicked, this, &DesQ::Panel::TaskButton::closeCurrentView );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );

    lyt->addWidget( iconLbl );
    lyt->addWidget( textLbl );
    lyt->addWidget( minBtn );
    lyt->addWidget( maxBtn );
    lyt->addWidget( closeBtn );

    setLayout( lyt );
}


void DesQ::Panel::TaskButton::handleOutputChanged( uint output_id ) {
    mCurrentOutput = output_id;

    QDBusReply<WorkSpace> replyWS = output->call( "QueryOutputWorkspace", output_id );

    if ( replyWS.isValid() ) {
        mCurrentWS = replyWS.value();
    }

    handleWorkspaceChanged( mCurrentOutput, mCurrentWS );
}


void DesQ::Panel::TaskButton::handleWorkspaceChanged( uint output_id, WorkSpace ws ) {
    /**
     * The workspace of the current output did not change.
     * No point in enumerating the views.
     */
    if ( mCurrentOutput != output_id ) {
        return;
    }

    /** For later use */
    mCurrentWS = ws;

    QDBusReply<QUIntList> replyViews = workspaces->call( "QueryActiveWorkspaceViews" );

    if ( replyViews.isValid() ) {
        mCurrentWorkspaceViews = replyViews.value();

        /** No Views, show the menu */
        if ( not mCurrentWorkspaceViews.count() ) {
            mCurrentView = 0;

            iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( mIconSize ) );
            textLbl->setText( "Applications" );

            minBtn->hide();
            maxBtn->hide();
            closeBtn->hide();

            return;
        }

        /** Views are there: get the one with focus */
        else {
            uint focusedView = 0;
            for ( uint viewId: mCurrentWorkspaceViews ) {
                QDBusReply<bool> replyFocus = views->call( "QueryViewActive", viewId );

                if ( replyFocus.isValid() ) {
                    if ( replyFocus.value() ) {
                        focusedView = viewId;
                    }
                }
            }

            if ( focusedView == 0 ) {
                mCurrentView = 0;

                iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( mIconSize ) );
                textLbl->setText( "Applications" );

                minBtn->hide();
                maxBtn->hide();
                closeBtn->hide();
            }

            else {
                setActive( focusedView );
            }
        }

        if ( mViews and mViews->isVisible() ) {
            mViews->close();
            delete mViews;

            createPreviewWidget();
        }
    }
}


void DesQ::Panel::TaskButton::setActive( uint viewId ) {
    if ( mCurrentView == viewId ) {
        return;
    }

    mCurrentView = viewId;

    QDBusReply<QString> replyAppId = views->call( "QueryViewAppId", viewId );
    QString             appId      = (replyAppId.isValid() ? replyAppId.value() : QString() );

    QDBusReply<QString> replyAppIdGtk = views->call( "QueryViewAppIdGtkShell", viewId );
    QString             appIdGtk      = (replyAppIdGtk.isValid() ? replyAppIdGtk.value() : QString() );

    QDBusReply<QString> replyTitle = views->call( "QueryViewTitle", viewId );
    QString             title      = (replyTitle.isValid() ? replyTitle.value() : QString() );

    if ( appId == "desq-menu" ) {
        iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( mIconSize ) );
        textLbl->setText( "Applications" );

        minBtn->hide();
        maxBtn->hide();
        closeBtn->show();

        return;
    }

    QIcon icon = getIconForAppId( appId );

    if ( icon.isNull() ) {
        icon = getIconForAppId( appIdGtk );
    }

    if ( icon.isNull() ) {
        icon = QIcon::fromTheme( "application-x-executable" );
    }

    iconLbl->setPixmap( icon.pixmap( mIconSize ) );
    textLbl->setText( title );

    minBtn->show();
    maxBtn->show();
    closeBtn->show();

    QDBusReply<bool> replyMax = views->call( "QueryViewMaximized", viewId );
    bool             isMax    = (replyMax.isValid() ? replyMax.value() : false);

    if ( isMax ) {
        maxBtn->setIcon( QIcon::fromTheme( "window-restore" ) );
    }

    else {
        maxBtn->setIcon( QIcon::fromTheme( "window-maximize" ) );
    }
}


void DesQ::Panel::TaskButton::addTask( uint viewId ) {
    if ( not mCurrentWorkspaceViews.contains( viewId ) ) {
        mCurrentWorkspaceViews << viewId;
    }
}


void DesQ::Panel::TaskButton::removeTask( uint viewId ) {
    mCurrentWorkspaceViews.removeAll( viewId );
    handleWorkspaceChanged( mCurrentOutput, mCurrentWS );
}


void DesQ::Panel::TaskButton::handleMaximizeChanged( uint viewId, bool yes ) {
    if ( mCurrentView != viewId ) {
        return;
    }

    if ( yes ) {
        maxBtn->setIcon( QIcon::fromTheme( "window-restore" ) );
    }

    else {
        maxBtn->setIcon( QIcon::fromTheme( "window-maximize" ) );
    }
}


void DesQ::Panel::TaskButton::handleMinimizeChanged( uint, bool ) {
    /** Currently we just want the button to be updated */
    handleWorkspaceChanged( mCurrentOutput, mCurrentWS );
}


void DesQ::Panel::TaskButton::handleStickyChanged( uint, bool ) {
    /** We don't have a UI for this at the moment */
}


void DesQ::Panel::TaskButton::updateButtonTitle( uint viewId, QString title ) {
    if ( mCurrentView == viewId ) {
        textLbl->setText( title );
    }
}


void DesQ::Panel::TaskButton::updateButtonIcon( uint viewId, QString appId ) {
    if ( mCurrentView != viewId ) {
        return;
    }

    if ( appId == "desq-menu" ) {
        iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( mIconSize ) );
        textLbl->setText( "Applications" );

        minBtn->hide();
        maxBtn->hide();
        closeBtn->show();

        return;
    }

    QIcon icon = getIconForAppId( appId );

    if ( icon.isNull() ) {
        QDBusReply<QString> replyAppIdGtk = views->call( "QueryViewAppIdGtkShell", viewId );
        QString             appIdGtk      = (replyAppIdGtk.isValid() ? replyAppIdGtk.value() : QString() );
        icon = getIconForAppId( appIdGtk );
    }

    if ( icon.isNull() ) {
        icon = QIcon::fromTheme( "application-x-executable" );
    }

    iconLbl->setPixmap( icon.pixmap( mIconSize ) );

    minBtn->show();
    maxBtn->show();
    closeBtn->show();
}


void DesQ::Panel::TaskButton::handleViewOutputChanged( uint viewId, uint from, uint to ) {
    /** The view was moved from the current view to a new view */
    if ( from == mCurrentOutput ) {
        removeTask( viewId );
    }

    /** A view was moved from somewhere to the current output's current workspace */
    else if ( to == mCurrentOutput ) {
        QDBusReply<WorkSpaces> replyWSs = workspaces->call( "QueryViewWorkspaces", viewId );

        if ( replyWSs.isValid() and replyWSs.value().contains( mCurrentWS ) ) {
            addTask( viewId );
        }
    }
}


void DesQ::Panel::TaskButton::handleViewWorkspaceChanged( uint viewId, WorkSpace from, WorkSpace to ) {
    QDBusReply<QUIntList> replyViews = output->call( "QueryActiveOutputViews" );

    /** The view is in the current output */
    if ( replyViews.isValid() and replyViews.value().contains( viewId ) ) {
        /** The view was moved from the current workspace */
        if ( from == mCurrentWS ) {
            removeTask( viewId );
        }

        /** The view was moved to the current workspace */
        else if ( to == mCurrentWS ) {
            addTask( viewId );
        }
    }
}


void DesQ::Panel::TaskButton::minimizeCurrentView() {
    /** No view to minimize */
    if ( mCurrentView == 0 ) {
        return;
    }

    views->call( "ToggleMinimizeView", mCurrentView );
}


void DesQ::Panel::TaskButton::maximizeCurrentView() {
    /** No view to maximize */
    if ( mCurrentView == 0 ) {
        return;
    }

    views->call( "ToggleMaximizeView", mCurrentView );
}


void DesQ::Panel::TaskButton::closeCurrentView() {
    /** No view to close */
    if ( mCurrentView == 0 ) {
        return;
    }

    views->call( "CloseView", mCurrentView );
}


void DesQ::Panel::TaskButton::createPreviewWidget() {
    if ( not mCurrentWorkspaceViews.count() ) {
        return;
    }

    mViews = new TasksPreview( mCurrentWorkspaceViews, mCurrentView );
    connect(
        mViews, &DesQ::Panel::TasksPreview::activate, [ = ] ( uint viewId ) {
            views->call( "FocusView", viewId );

            mViews->close();
            delete mViews;

            mViews = nullptr;
        }
    );

    emit showViewsWidget( mViews );
}


void DesQ::Panel::TaskButton::enterEvent( QEvent *event ) {
    setCursor( Qt::PointingHandCursor );
    event->accept();
}


void DesQ::Panel::TaskButton::leaveEvent( QEvent *event ) {
    setCursor( Qt::ArrowCursor );
    event->accept();
}


void DesQ::Panel::TaskButton::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
        longPressTimer->start( 400, Qt::PreciseTimer, this );
    }

    mEvent->accept();
}


void DesQ::Panel::TaskButton::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        longPressTimer->stop();

        /** Not a long-press */
        if ( mPressed ) {
            /** Nothing in focus right now. So, show the applications */
            if ( mCurrentView == 0 ) {
                QProcess::startDetached( DesQ::Utils::getUtilityPath( "menu" ), {} );
            }

            /** Some view is in focus, show the views */
            else {
                if ( mViews and mViews->isVisible() ) {
                    mViews->close();
                    delete mViews;
                    mViews = nullptr;
                }

                else {
                    createPreviewWidget();
                }
            }
        }
    }

    mEvent->accept();
}


void DesQ::Panel::TaskButton::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == longPressTimer->timerId() ) {
        longPressTimer->stop();
        mPressed = false;

        /** Nothing in focus right now. So, show the views */
        if ( mCurrentView == 0 ) {
            createPreviewWidget();
        }

        /** Some view is in focus, show applications */
        else {
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "menu" ), {} );
        }
    }

    QWidget::timerEvent( tEvent );
}


void DesQ::Panel::TaskButton::wheelEvent( QWheelEvent *wEvent ) {
    /** No point in trying to switch views when there is only one view */
    if ( mCurrentWorkspaceViews.count() <= 1 ) {
        return;
    }

    QPoint numDegrees = wEvent->angleDelta() / 8;

    int x = fabs( numDegrees.x() );
    int y = fabs( numDegrees.y() );

    int steps = 0;

    if ( x > y ) {
        steps = numDegrees.x();
    }

    else {
        steps = numDegrees.y();
    }

    int curIdx = mCurrentWorkspaceViews.indexOf( mCurrentView );

    if ( curIdx < 0 ) {
        wEvent->ignore();
        return;
    }

    mSteps += steps;

    qDebug() << numDegrees << steps << mSteps << ( int )shellSett->value( "Panel/TasksScrollThreshold" );

    if ( fabs( mSteps ) < ( int )shellSett->value( "Panel/TasksScrollThreshold" ) ) {
        wEvent->ignore();
        return;
    }

    /** Go to the next view */
    if ( mSteps > 0 ) {
        /** Last in the list: So go to the first */
        if ( curIdx == mCurrentWorkspaceViews.count() - 1 ) {
            curIdx = 0;
        }

        else {
            curIdx++;
        }
    }

    /** Go to the previous view */
    else {
        /** First view */
        if ( curIdx == 0 ) {
            curIdx = mCurrentWorkspaceViews.count() - 1;
        }

        else {
            curIdx--;
        }
    }

    views->call( "FocusView", mCurrentWorkspaceViews.at( curIdx ) );
    mSteps = 0;
}
