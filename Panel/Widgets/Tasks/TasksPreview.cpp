/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "MiscTools.hpp"
#include "TasksPreview.hpp"

DesQ::Panel::Task::Task( uint viewId ) : QWidget() {
    mViewId = viewId;

    compositor->call( "CaptureView", viewId, QString( "/tmp/%1.png" ).arg( viewId ) );

    do {
        mThumb = QImage( QString( "/tmp/%1.png" ).arg( viewId ) );
        QThread::msleep( 5 );
        qApp->processEvents();
    } while ( mThumb.isNull() );

    mThumb = mThumb.scaled( QSize( 256, 144 ), Qt::KeepAspectRatio, Qt::SmoothTransformation );

    QDBusReply<QString> reply = views->call( "QueryViewTitle", viewId );

    mTitle = reply.value();

    reply = views->call( "QueryViewAppId", viewId );
    mIcon = getIconForAppId( reply.value() ).pixmap( 28 );

    QDBusReply<bool> replyB = views->call( "QueryViewActive", viewId );

    mActive = (replyB.isValid() ? replyB.value() : false);

    setFixedSize( 256, 180 );

    setMouseTracking( true );
}


DesQ::Panel::Task::~Task() {
}


void DesQ::Panel::Task::enterEvent( QEvent *event ) {
    mHover = true;
    event->accept();

    setCursor( Qt::PointingHandCursor );

    repaint();
}


void DesQ::Panel::Task::leaveEvent( QEvent *event ) {
    mHover = false;
    event->accept();

    setCursor( Qt::ArrowCursor );

    repaint();
}


void DesQ::Panel::Task::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void DesQ::Panel::Task::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( not mPressed ) {
        mEvent->accept();
        return;
    }

    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = false;

        emit activate( mViewId );
    }

    mEvent->accept();
}


void DesQ::Panel::Task::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    QRectF r1( (256 - mThumb.width() ) / 2, (144 - mThumb.height() ) / 2, mThumb.width(), mThumb.height() );
    QRectF r2( 4, 148, 28, 28 );
    QRectF r3( 36, 144, 220, 36 );

    QColor highlight( palette().color( QPalette::Highlight ) );

    if ( mActive ) {
        highlight.setAlphaF( 0.2 );
        painter.setBrush( highlight );
        painter.drawRect( rect() );

        painter.fillRect( r1, Qt::transparent );
    }

    else if ( mHover ) {
        highlight.setAlphaF( 0.1 );
        painter.setBrush( highlight );
        painter.drawRect( rect() );

        painter.fillRect( r1, Qt::transparent );
    }

    painter.drawImage( r1, mThumb );
    painter.drawPixmap( r2.toRect(), mIcon );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( palette().color( QPalette::Text ) );
    painter.drawText( r3, Qt::AlignVCenter | Qt::AlignLeft, mTitle );

    if ( mActive ) {
        highlight.setAlphaF( 1.0 );
        painter.setPen( QPen( highlight, 2 ) );
        painter.drawRect( rect().adjusted( 1, 1, -1, -1 ) );
    }

    else if ( mHover ) {
        highlight.setAlphaF( 1.0 );
        painter.setPen( QPen( highlight, 2 ) );
        painter.drawRect( rect().adjusted( 1, 1, -1, -1 ) );
    }

    painter.end();

    pEvent->accept();
}


DesQ::Panel::TasksPreview::TasksPreview( QUIntList views, uint activeId ) : QWidget() {
    setContentsMargins( QMargins() );

    QScrollArea *scroll = new QScrollArea( this );

    scroll->setFrameStyle( QFrame::NoFrame );

    scroll->setFixedHeight( 200 );
    scroll->setMinimumWidth( 256 + 20 );
    scroll->setMaximumWidth( 808 );

    scroll->setContentsMargins( QMargins() );
    scroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    scroll->setWidgetResizable( true );

    QWidget *base = new QWidget();

    base->setFixedHeight( 200 );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setSpacing( 10 );
    lyt->setContentsMargins( QMargins( 10, 10, 10, 10 ) );

    base->setLayout( lyt );
    scroll->setWidget( base );

    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->setContentsMargins( QMargins( 2, 2, 2, 2 ) );
    baseLyt->setSpacing( 0 );
    baseLyt->addWidget( scroll );
    setLayout( baseLyt );

    int col = 0;

    for ( uint viewId: views ) {
        Task *taskW = new Task( viewId );
        connect( taskW, &DesQ::Panel::Task::activate, this, &DesQ::Panel::TasksPreview::activate );

        lyt->addWidget( taskW );

        taskIds << viewId;
        tasks << taskW;

        col++;
    }

    curIdx = taskIds.indexOf( activeId );

    base->setFixedWidth( col * 256 + (col - 1) * 10 + 20 );

    setFixedHeight( 204 );

    if ( col == 1 ) {
        scroll->setFixedWidth( 276 );
        setFixedWidth( 280 );
    }

    else if ( col == 2 ) {
        scroll->setFixedWidth( 542 );
        setFixedWidth( 546 );
    }

    else {
        scroll->setFixedWidth( 808 );
        setFixedWidth( 812 );
    }

    setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    setAttribute( Qt::WA_TranslucentBackground );
}


DesQ::Panel::TasksPreview::~TasksPreview() {
    for ( Task *task: tasks ) {
        delete task;
    }

    taskIds.clear();
    tasks.clear();
}


void DesQ::Panel::TasksPreview::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        close();
    }
}


void DesQ::Panel::TasksPreview::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    painter.setPen( QPen( Qt::black, 2.0 ) );
    painter.setBrush( QColor( 0, 0, 0, 180 ) );
    painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 5.0, 5.0 );
    painter.end();

    QWidget::paintEvent( pEvent );
}
