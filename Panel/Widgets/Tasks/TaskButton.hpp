/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtDBus>
#include <QtWidgets>

#include <wayqt/WindowManager.hpp>

#include "dbustypes.hpp"

namespace DesQ {
    namespace Panel {
        class TaskButton;
        class TasksPreview;
    }
}

class DesQ::Panel::TaskButton : public QWidget {
    Q_OBJECT;

    public:
        TaskButton( QWidget *parent );

    private:
        uint mCurrentOutput  = 0;
        uint mCurrentView    = 0;
        WorkSpace mCurrentWS = { 0, 0 };
        QList<uint> mCurrentWorkspaceViews;

        /** IconSize */
        int mIconSize = 28;

        QLabel *iconLbl;
        QLabel *textLbl;

        QToolButton *closeBtn;
        QToolButton *maxBtn;
        QToolButton *minBtn;

        /** TasksPreview Widget */
        TasksPreview *mViews = nullptr;

        /** Flag to ensure mouse press was performed in this widgets */
        bool mPressed = false;

        /** A long-press timer */
        QBasicTimer *longPressTimer;

        /** Number of steps to scroll before switching views */
        int mSteps = 0;

        /** Build the UI */
        void buildUI();

        Q_SLOT void handleOutputChanged( uint );
        Q_SLOT void handleWorkspaceChanged( uint, WorkSpace );

        Q_SLOT void setActive( uint viewId );

        Q_SLOT void addTask( uint );
        Q_SLOT void removeTask( uint );

        /** Slots that handle view state changes */
        Q_SLOT void handleMaximizeChanged( uint, bool );
        Q_SLOT void handleMinimizeChanged( uint, bool );
        Q_SLOT void handleStickyChanged( uint, bool );

        Q_SLOT void updateButtonTitle( uint, QString );
        Q_SLOT void updateButtonIcon( uint, QString );

        /** View location changes: output and workspace */
        Q_SLOT void handleViewOutputChanged( uint vid, uint from, uint to );
        Q_SLOT void handleViewWorkspaceChanged( uint vid, WorkSpace from, WorkSpace to );

        void minimizeCurrentView();
        void maximizeCurrentView();
        void closeCurrentView();

        void createPreviewWidget();

    protected:
        void enterEvent( QEvent * );
        void leaveEvent( QEvent * );

        void mousePressEvent( QMouseEvent * );
        void mouseReleaseEvent( QMouseEvent * );

        void timerEvent( QTimerEvent * );

        void wheelEvent( QWheelEvent * );

    Q_SIGNALS:
        void showViewsWidget( QWidget * );
};
