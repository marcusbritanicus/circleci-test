/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QGridLayout>

#include "dbustypes.hpp"

class QDBusInterface;

class WorkspaceWidget : public QWidget {
    Q_OBJECT;

    public:
        WorkspaceWidget( WorkSpace, int number, uint output, QWidget * );

        WorkSpace workSpace();

        void highlight( bool );

    private:
        WorkSpace mCurWS;
        int mNumber;
        uint mOutput;
        bool mPressed   = false;
        bool mHighlight = false;

    protected:
        void mousePressEvent( QMouseEvent *mEvent );
        void mouseReleaseEvent( QMouseEvent *mEvent );

        void paintEvent( QPaintEvent *pEvent );

    Q_SIGNALS:
        void switchWorkSpace( WorkSpace );
};

class PagerWidget : public QWidget {
    Q_OBJECT

    public:
        PagerWidget( QWidget *parent = 0 );

    private:
        QDBusInterface *compositor;
        QDBusInterface *output;

        QGridLayout *wsLyt;

        uint mCurOutput;
        QList<WorkspaceWidget *> workspaces;

    private Q_SLOTS:
        void highlightWorkspace( uint, WorkSpace );
        void populateLayout( uint outputid );
};
