/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "PagerWidget.hpp"

WorkspaceWidget::WorkspaceWidget( WorkSpace ws, int number, uint output, QWidget *parent ) : QWidget( parent ) {
    mCurWS  = ws;
    mNumber = number;
    mOutput = output;
    setFixedSize( QSize( 32, 24 ) );
}


WorkSpace WorkspaceWidget::workSpace() {
    return mCurWS;
}


void WorkspaceWidget::highlight( bool yes ) {
    mHighlight = yes;
    repaint();
}


void WorkspaceWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void WorkspaceWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( (mEvent->button() == Qt::LeftButton) and mPressed ) {
        /**
         * If we're highlighted, then this is the current workspace.
         * We toggle "ShowDesktop"
         */
        if ( mHighlight ) {
            output->call( "ShowDesktop" );
        }

        else {
            QDBusReply<void> reply = output->call( "ChangeWorkspace", mOutput, QVariant::fromValue( mCurWS ) );

            if ( reply.isValid() ) {
                mHighlight = true;
            }

            else {
                qWarning() << reply.error().message();
            }
        }
    }

    repaint();
    mEvent->accept();
}


void WorkspaceWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();

    if ( mHighlight ) {
        QColor clr( palette().color( QPalette::Highlight ) );
        clr = clr.darker();
        painter.setPen( QPen( clr, 1.0 ) );
        clr = clr.lighter();
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    else {
        QColor clr( palette().color( QPalette::Window ) );
        painter.setPen( QPen( clr, 1.0 ) );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    painter.restore();

    painter.save();

    if ( mHighlight ) {
        QFont f( font() );
        f.setWeight( QFont::Bold );
        painter.setFont( f );
    }

    painter.drawText( rect(), Qt::AlignCenter, QString::number( mNumber ) );
    painter.restore();

    painter.end();

    pEvent->accept();
}


PagerWidget::PagerWidget( QWidget *parent ) : QWidget( parent ) {
    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceGrid>( "WorkSpaceGrid" );
    qDBusRegisterMetaType<WorkSpaceGrid>();

    compositor = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor", QDBusConnection::sessionBus() );
    output     = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.output", QDBusConnection::sessionBus() );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire",
        "/org/DesQ/Wayfire",
        "wayland.compositor",
        "OutputWorkspaceChanged",
        "u(ii)",
        this,
        SLOT( highlightWorkspace( uint, WorkSpace ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire",
        "/org/DesQ/Wayfire",
        "wayland.compositor",
        "OutputChanged",
        "u",
        this,
        SLOT( populateLayout( uint ) )
    );

    wsLyt = new QGridLayout();
    setLayout( wsLyt );

    QDBusReply<uint> opReply = output->call( "QueryActiveOutput" );

    if ( opReply.isValid() ) {
        populateLayout( opReply.value() );
    }
}


void PagerWidget::populateLayout( uint output_id ) {
    mCurOutput = output_id;

    /* Delete the previously added ClipboardItem */
    if ( wsLyt->count() ) {
        qDeleteAll( wsLyt->findChildren<WorkspaceWidget *>() );
    }

    if ( workspaces.count() ) {
        qDeleteAll( workspaces );
    }

    workspaces.clear();

    QDBusReply<WorkSpaceGrid> wsGrid = output->call( "QueryOutputWorkspaceGrid", output_id );

    if ( wsGrid.isValid() ) {
        WorkSpaceGrid wsg = wsGrid.value();

        int wsCount = 1;
        for ( int r = 0; r < wsg.rows; r++ ) {
            for ( int c = 0; c < wsg.columns; c++ ) {
                WorkspaceWidget *wsw = new WorkspaceWidget( { r, c }, wsCount, mCurOutput, this );
                wsLyt->addWidget( wsw, r, c );
                workspaces << wsw;

                wsCount++;
            }
        }
    }

    QDBusReply<WorkSpace> wsx = output->call( "QueryOutputWorkspace", mCurOutput );

    if ( wsx.isValid() ) {
        highlightWorkspace( mCurOutput, wsx.value() );
    }
}


void PagerWidget::highlightWorkspace( uint output_id, WorkSpace ws ) {
    if ( output_id != mCurOutput ) {
        return;
    }

    for ( WorkspaceWidget *wsw: workspaces ) {
        if ( wsw->workSpace() == ws ) {
            wsw->highlight( true );
        }

        else {
            wsw->highlight( false );
        }
    }
}
